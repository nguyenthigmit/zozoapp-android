package com.zozo.deeplink;

import android.content.UriMatcher;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.zozo.utils.StringUtils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DeepLinker {

    private static final String KEY_LINK = "link";
    public static final String KEY_CODE = "Code";
    public static final String KEY_USER_ID = "userId";
    public static final String KEY_CONFIRM_URL = "confirm_url";
    private static final String AUTHORITY = "*";
    private static final String PATH_ID = "/*";

    public enum Link {
        HOME(null),
        EMAIL_CONFIRM("EmailConfirm" + PATH_ID);
        final String path;

        Link(String path) {
            this.path = path;
        }
    }

    public static Link getLinkFromBundle(Bundle bundle) {
        Link link = null;
        int val = bundle.getInt(KEY_LINK, -1);
        if (val > -1 && val < Link.values().length) {
            link = Link.values()[val];
        }
        return link;
    }

    public static long getIdFromBundle(Bundle bundle) {
        return bundle.getLong(KEY_USER_ID);
    }

    private final UriMatcher mUriMatcher;

    public DeepLinker() {
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        // Add all links to the matcher
        for (Link link : Link.values()) {
            mUriMatcher.addURI(AUTHORITY, link.path, link.ordinal());
        }
    }

    /**
     * Builds a bundle from the given URI
     */
    public Bundle buildBundle(Uri uri) {
        int code = mUriMatcher.match(uri);

        // Default to home
        Link link = Link.HOME;

        if (code == UriMatcher.NO_MATCH) {
            // Revert code to match default link
            code = link.ordinal();
        } else {
            // Update default link with the matched one
            link = Link.values()[code];
        }

        Bundle bundle = new Bundle();
        bundle.putInt(KEY_LINK, code);

        switch (link) {
            case HOME:
                break;
            case EMAIL_CONFIRM:
                Map<String, List<String>> query = StringUtils.splitQuery(uri);
                bundle.putString(KEY_CONFIRM_URL, uri.toString());
                bundle.putSerializable(KEY_USER_ID, new ArrayList<String>(Objects.requireNonNull(query.get(KEY_USER_ID))));
                bundle.putSerializable(KEY_CODE, new ArrayList<String>(Objects.requireNonNull(query.get(KEY_CODE))));
                break;
        }

        return bundle;
    }


}
