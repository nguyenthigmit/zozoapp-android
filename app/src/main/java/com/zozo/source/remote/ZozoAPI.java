package com.zozo.source.remote;

import com.zozo.model.BoothItemData;
import com.zozo.model.CheckValidCodeParams;
import com.zozo.model.CreateNewPasswordParams;
import com.zozo.model.CreatePhotoParams;
import com.zozo.model.CreatePhotoResponse;
import com.zozo.model.CreateShopParams;
import com.zozo.model.CreateUserParams;
import com.zozo.model.CreateUserResponse;
import com.zozo.model.ForgotPassworkParams;
import com.zozo.model.LoginParams;
import com.zozo.model.ResponseModel;
import com.zozo.model.User;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ZozoAPI {

    @POST("Auth/Login")
    Observable<ResponseModel<Map<String, String>>> login(@Body LoginParams params);

    @GET("Auth/Me")
    Observable<User> me(@Header("Authorization") String token, @Query("userId") String userId, @Query("latitude") Double latitude, @Query("longitude") Double longitude, @Query("address") String address);

    @GET("EmailConfirm/EmailConfirm")
    Observable<String> emailConfirm(@Query("Code") String code, @Query("userId") String userId);

    @POST("User/CreateNewPassword")
    Observable<ResponseModel<String>> createNewPassword(@Body CreateNewPasswordParams params);

    @POST("User/CheckValidCode")
    Observable<ResponseModel<String>> checkValidCode(@Body CheckValidCodeParams params);

    @POST("User/ForgotPassword")
    Observable<ResponseModel<ForgotPassworkParams>> forgotPassword(@Body Map<String, String> params);

    @POST("User/Create")
    Observable<ResponseModel<CreateUserResponse>> createUser(@Body CreateUserParams params);

    @POST("User/ConfirmAccount")
    Observable<ResponseModel<String>> confirmAccount(@Body Map<String, String> params);

    @POST("User/Update")
    Observable<ResponseModel<String>> updateUser(@Body CreateUserParams params);

    @GET("Shop/GetShopByUserId")
    Observable<ResponseModel<List<BoothItemData>>> getShopByUserId(@Header("Authorization") String token);

    @POST("Shop/Create")
    Observable<ResponseModel<String>> createShop(@Header("Authorization") String token, @Body CreateShopParams params);

    @PUT("Shop/Update")
    Observable<ResponseModel<String>> updateShop(@Header("Authorization") String token, @Body CreateShopParams params);

    @POST("Photo/Create")
    Observable<ResponseModel<CreatePhotoResponse>> createPhoto(@Header("Authorization") String token, @Body CreatePhotoParams params);


}
