package com.zozo.source.remote;

import android.annotation.SuppressLint;

import com.zozo.model.BoothItemData;
import com.zozo.model.CheckValidCodeParams;
import com.zozo.model.CreateNewPasswordParams;
import com.zozo.model.CreatePhotoParams;
import com.zozo.model.CreatePhotoResponse;
import com.zozo.model.CreateShopParams;
import com.zozo.model.CreateUserParams;
import com.zozo.model.CreateUserResponse;
import com.zozo.model.ForgotPassworkParams;
import com.zozo.model.LoginParams;
import com.zozo.model.ResponseModel;
import com.zozo.model.User;
import com.zozo.source.TaskDataSource;
import com.zozo.source.local.TaskLocalDataSource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

public class TaskRemoteDataSource implements TaskDataSource {

    @SuppressLint("StaticFieldLeak")
    private static volatile TaskRemoteDataSource INSTANCE;

    private final ZozoAPI zozoAPI;

    private TaskRemoteDataSource(ZozoAPI zozoAPI) {
        this.zozoAPI = zozoAPI;
    }

    public static TaskRemoteDataSource getInstance(ZozoAPI zozoAPI) {
        if (INSTANCE == null) {
            synchronized (TaskLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new TaskRemoteDataSource(zozoAPI);
                }
            }
        }
        return INSTANCE;
    }


    @Override
    public User getUserProfile() {
        return null;
    }

    @Override
    public void persistAuth(String token, String userId) {

    }

    public Observable<User> me(String token, String userId, Double latitude, Double longitude, String address) {
        return zozoAPI.me(token, userId, latitude, longitude, address);
    }

    public Observable<ResponseModel<Map<String, String>>> executeLogin(String username, String password) {
        return zozoAPI.login(new LoginParams(username, password));
    }

    public Observable<ResponseModel<CreateUserResponse>> createUser(CreateUserParams params) {
        return zozoAPI.createUser(params);
    }

    public Observable<ResponseModel<String>> confirmAccount(String userId, String code) {

        Map<String, String> params = new HashMap<>();
        params.put("UserId", userId);
        params.put("Code", code);

        return zozoAPI.confirmAccount(params);
    }

    public Observable<String> emailConfirm(String code, String userId) {
        return zozoAPI.emailConfirm(code, userId);
    }

    public Observable<ResponseModel<ForgotPassworkParams>> forgotPassword(String email, String phone) {
        Map<String, String> params = new HashMap<>();
        params.put("Email", email);
        params.put("PhoneNumber", phone);

        return zozoAPI.forgotPassword(params);
    }

    public Observable<ResponseModel<String>> createNewPassword(String password, String userId, String code) {
        return zozoAPI.createNewPassword(new CreateNewPasswordParams(password, userId, code));
    }

    public Observable<ResponseModel<String>> checkValidCode(String userId, String code) {
        return zozoAPI.checkValidCode(new CheckValidCodeParams(userId, code));
    }

    public Observable<ResponseModel<List<BoothItemData>>> getShopByUserId(String token) {
        return zozoAPI.getShopByUserId(token);
    }

    public Observable<ResponseModel<String>> createShop(String token, CreateShopParams params)  {
        return zozoAPI.createShop(token, params);
    }

    public Observable<ResponseModel<String>> updateShop(String token, CreateShopParams params)  {
        return zozoAPI.updateShop(token, params);
    }

    public Observable<ResponseModel<CreatePhotoResponse>> createPhoto(String token, CreatePhotoParams params) {
        return zozoAPI.createPhoto(token, params);
    }

}
