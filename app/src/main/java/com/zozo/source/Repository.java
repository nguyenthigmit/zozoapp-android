package com.zozo.source;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.zozo.event.Event;
import com.zozo.model.AuthToken;
import com.zozo.model.BoothItemData;
import com.zozo.model.User;
import com.zozo.source.local.TaskLocalDataSource;
import com.zozo.source.remote.TaskRemoteDataSource;

import java.util.List;

import static com.zozo.constant.SharePreConstant.ACCESS_TOKEN;
import static com.zozo.constant.SharePreConstant.ROOT_SHARE_PRE;

public class Repository implements TaskDataSource {

    private final TaskDataSource taskRemoteDataSource;
    private final TaskDataSource taskLocalDataSource;

    // must remove in short time because leak memory issue
    private Context context;
    private SharedPreferences sharedPreferences;

    public User user;
    public AuthToken authToken;
    public MutableLiveData<Event<List<BoothItemData>>> listBoothLiveData = new MutableLiveData<>();


    public Repository(@NonNull TaskDataSource taskRemoteDataSource, @NonNull TaskDataSource taskLocalDataSource, Context context) {
        this.taskRemoteDataSource = taskRemoteDataSource;
        this.taskLocalDataSource = taskLocalDataSource;
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(ROOT_SHARE_PRE, Context.MODE_PRIVATE);

        String token = this.sharedPreferences.getString("token", null);
        String userId = this.sharedPreferences.getString("userId", null);
        this.authToken = new AuthToken(token, userId);

    }

    public TaskLocalDataSource getTaskLocalDataSource() {
        return (TaskLocalDataSource) this.taskLocalDataSource;
    }

    public TaskRemoteDataSource getTaskRemoteDataSource() {
        return (TaskRemoteDataSource) this.taskRemoteDataSource;
    }


    @Override
    public User getUserProfile() {
        String token = sharedPreferences.getString(ACCESS_TOKEN, null);

        if (token == null) {
            return null;
        }

        return this.taskRemoteDataSource.getUserProfile();
    }

    @Override
    public void persistAuth(String token, String userId) {
        this.authToken = new AuthToken("Bearer " +token, userId);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("token", "Bearer " + token);
        edit.putString("userId", userId);
        edit.apply();
    }

    public AuthToken getAuthToken() {
        return this.authToken;
    }

    public void logout() {

        authToken = new AuthToken();
        listBoothLiveData = new MutableLiveData<>();
        user = new User();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", null);
        editor.putString("userId", null);
        editor.apply();
    }

}
