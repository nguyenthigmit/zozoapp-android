package com.zozo.source.local;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import com.zozo.model.User;
import com.zozo.source.TaskDataSource;

public class TaskLocalDataSource implements TaskDataSource {

    @SuppressLint("StaticFieldLeak")
    private static volatile TaskLocalDataSource INSTANCE;
    private SharedPreferences sharedPreferences;

    private TaskLocalDataSource(){
    }

    public static TaskLocalDataSource getInstance() {
        if (INSTANCE == null) {
            synchronized (TaskLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new TaskLocalDataSource();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public User getUserProfile() {
        return null;
    }

    @Override
    public void persistAuth(String token, String userId) {

    }

}
