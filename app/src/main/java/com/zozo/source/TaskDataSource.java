package com.zozo.source;

import com.zozo.model.User;


public interface TaskDataSource {

    User getUserProfile();
    void persistAuth(String token, String userId);
}
