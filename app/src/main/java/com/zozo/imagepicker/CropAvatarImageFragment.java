package com.zozo.imagepicker;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.yalantis.ucrop.callback.BitmapCropCallback;
import com.yalantis.ucrop.view.UCropView;
import com.zozo.R;
import com.zozo.constant.KeyConstant;
import com.zozo.event.Event;
import com.zozo.utils.ApiResponse;

import java.io.File;
import java.net.URL;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.ymex.popup.controller.ProgressController;
import cn.ymex.popup.dialog.PopupDialog;

import static android.app.Activity.RESULT_OK;

public class CropAvatarImageFragment extends Fragment implements Toolbar.OnMenuItemClickListener {

    public static int IMAGE_CROP_CODE = 0x102;

    @BindView(R.id.ucrop_view)
    UCropView uCropView;

    @BindView(R.id.toolbar)
    MaterialToolbar toolbar;

    CropImageViewModel viewModel;

    PopupDialog popupDialog;


    public static CropAvatarImageFragment newInstance(Uri uri, Integer boothId) {
        CropAvatarImageFragment cropAvatarImageFragment = new CropAvatarImageFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KeyConstant.URI, uri);
        bundle.putInt(KeyConstant.ID, boothId);
        cropAvatarImageFragment.setArguments(bundle);
        return cropAvatarImageFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.crop_avatar_image_fm, container, false);
        ButterKnife.bind(this, view);
        viewModel = ((CropImageActivity) getActivity()).viewModel;
        viewModel.createAvatarPhotoResponse.observe(this, this::handleCreatePhoto);
        popupDialog = PopupDialog.create(getContext());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        toolbar.inflateMenu(R.menu.image_crop_menu);
        toolbar.setOnMenuItemClickListener(this);
        this.handleArgument();
        return view;
    }

    private void handleArgument() {
        Uri uri = getArguments().getParcelable(KeyConstant.URI);

        if (uri == null) {
            getActivity().finish();
        }

        try {
            (uCropView.getCropImageView()).setImageUri(uri, Uri.fromFile(new File(getActivity().getCacheDir(), "TRUNG.jpg")));
            this.setupAvatarType();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setupAvatarType() {
        uCropView.getCropImageView().setImageToWrapCropBounds(true);
        uCropView.getCropImageView().setScaleType(ImageView.ScaleType.FIT_END);
        uCropView.getCropImageView().setRotateEnabled(false);
        uCropView.getCropImageView().setScaleEnabled(true);
        uCropView.getOverlayView().setCircleDimmedLayer(true);
        uCropView.getOverlayView().setShowCropGrid(false);
        uCropView.getOverlayView().setShowCropFrame(false);
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_image_crop:
                uCropView.getCropImageView().cropAndSaveImage(Bitmap.CompressFormat.JPEG, 96, new BitmapCropCallback() {

                    @Override
                    public void onBitmapCropped(@NonNull Uri resultUri, int offsetX, int offsetY, int imageWidth, int imageHeight) {
                        Integer id = getArguments().getInt(KeyConstant.ID, 0);

                        // create photo sever
                        viewModel.createPhotoAvatar(resultUri, getContext().getContentResolver(), id);
                    }

                    @Override
                    public void onCropFailure(@NonNull Throwable t) {
                    }
                });
                return true;
        }

        return false;
    }

    public void cropImageSuccess(Uri resultUri) {
        Intent intent = new Intent();
        intent.setData(resultUri);
        getActivity().setResult(RESULT_OK, intent);
        getActivity().finish();
    }

    private void handleCreatePhoto(Event<ApiResponse> event) {

        ApiResponse apiResponse = event.getContentIfNotHandled();

        if (apiResponse == null) {
            return;
        }

        switch (apiResponse.status) {
            case LOADING:
                popupDialog.setBackgroundDrawable(getResources().getDrawable(R.drawable.error_rounded_edittext));
                popupDialog.controller(ProgressController.build().message(getResources().getString(R.string.loading)))
                        .backgroundDrawable(new ColorDrawable(Color.parseColor("#66000000")))
                        .show();
                break;
            case SUCCESS:
                popupDialog.dismiss();
                cropImageSuccess((Uri) apiResponse.data);
                break;
            case ERROR:
                popupDialog.dismiss();
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
                builder.setMessage(Objects.requireNonNull(apiResponse.error).getMessage());
                builder.setNegativeButton(getResources().getText(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
                break;
        }


    }
}
