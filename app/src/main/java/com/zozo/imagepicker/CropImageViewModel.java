package com.zozo.imagepicker;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fenchtose.nocropper.ImageCache;
import com.github.underscore.U;
import com.zozo.constant.Urls;
import com.zozo.event.Event;
import com.zozo.model.BoothItemData;
import com.zozo.model.CreatePhotoParams;
import com.zozo.model.CreateShopParams;
import com.zozo.source.Repository;
import com.zozo.utils.ApiResponse;
import com.zozo.utils.BitmapUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CropImageViewModel extends ViewModel {

    Repository repository;

    public final MutableLiveData<Event<ApiResponse>> createCoverPhotoResponse = new MutableLiveData<>();
    public final MutableLiveData<Event<ApiResponse>> createAvatarPhotoResponse = new MutableLiveData<>();
    public final MutableLiveData<Event<ApiResponse>> updateShopResponse = new MutableLiveData<>();

    private final CompositeDisposable disposables = new CompositeDisposable();


    public CropImageViewModel(Repository repository) {
        this.repository = repository;
    }

    public void createPhotoCover(Integer boothId, File root, URL url, int byteCount) {


        final CreateShopParams[] temp = new CreateShopParams[1];

        createCoverPhotoResponse.setValue(new Event<>(ApiResponse.loading()));
        disposables.add(Observable.just(new ImageCache(root, byteCount).readFromDiskCache(url))
                .subscribeOn(Schedulers.io())
                .flatMap(bitmap -> {
                    if (!BitmapUtils.checkSizeBitmap(bitmap, 2000000)) {
                        throw new Exception("Bitmap too large. Bitmap must low 2MB");
                    }
                    return Observable.just(BitmapUtils.encodeTobase64(bitmap));
                })
                .flatMap(base64 -> {
                    // create photo params
                    CreatePhotoParams params = new CreatePhotoParams();
                    String uuid = UUID.randomUUID().toString();
                    params.setBase64String(base64);
                    params.setName(uuid);
                    params.setAlt(uuid);

                    //TODO: enum for clean code
                    params.setPhotoTypeId(1);
                    params.setStatus(1);

                    return Observable.just(params);
                })
                .flatMap(params -> repository.getTaskRemoteDataSource().createPhoto(repository.authToken.token, params))
                .flatMap(response -> {
                    if (response.isSuccess()) {
                        return Observable.just(response.getData());
                    }
                    throw new Exception(response.getMessage());
                }).flatMap(createPhoto -> {
                    final BoothItemData[] boothItemData = new BoothItemData[1];
                    List<BoothItemData> listBooth = Objects.requireNonNull(repository.listBoothLiveData.getValue()).getContent();
                    for (int i = 0; i < listBooth.size(); i++) {
                        if (boothId.equals(listBooth.get(i).getId())) {
                            boothItemData[0] = listBooth.get(i);
                        }
                    }

                    if (boothItemData[0] == null) {
                        throw new Exception("Not found any booth that match with booth id in list booth");
                    }

                    CreateShopParams createShopParams = new CreateShopParams();
                    createShopParams.setId(boothItemData[0].getId());
                    createShopParams.setCode(boothItemData[0].getCode());
                    createShopParams.setName(boothItemData[0].getName());
                    createShopParams.setAddress(boothItemData[0].getAddress());
                    createShopParams.setHotline(boothItemData[0].getHotline());
                    createShopParams.setMobile(boothItemData[0].getMobile());
                    createShopParams.setWebsite(boothItemData[0].getWebsite());

                    createShopParams.setAvatar(boothItemData[0].getAvatar());
                    createShopParams.setCardImage(Urls.HOST + createPhoto.getUrl());

                    temp[0] = createShopParams;
                    int index = U.findIndex(listBooth, arg -> boothId.equals(arg.getId()));
                    BoothItemData data = listBooth.get(index);

                    data.setCode(temp[0].getCode());
                    data.setName(temp[0].getName());
                    data.setAddress(temp[0].getAddress());
                    data.setWebsite(temp[0].getWebsite());
                    data.setMap(temp[0].getMap());
                    data.setHotline(temp[0].getHotline());
                    data.setMobile(temp[0].getMobile());
                    data.setAvatar(temp[0].getAvatar());
                    data.setCardImage(temp[0].getCardImage());

                    Map<String, Object> resultData = new HashMap<>();
                    resultData.put("byteCount", byteCount);
                    resultData.put("url", url);

                    createCoverPhotoResponse.postValue(new Event<>(ApiResponse.success(resultData)));

                    return repository.getTaskRemoteDataSource().updateShop(repository.authToken.token, createShopParams);

                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) ->
                        createCoverPhotoResponse.setValue(new Event<>(ApiResponse.loading())))
                .subscribe(
                        result -> {

                            if (temp[0] == null) {
                                throw new Exception("Not found CreateShopParams");
                            }

                            if (result.isSuccess()) {
                                updateShopResponse.setValue(new Event<>(ApiResponse.success(result)));
                            } else {
                                updateShopResponse.setValue(new Event<>(ApiResponse.error(new Throwable(result.getMessage()))));
                            }

                        },
                        throwable -> {
                            updateShopResponse.setValue(new Event<>(ApiResponse.error(throwable)));
                        }
                ));

    }

    public void createPhotoAvatar(Uri uri, ContentResolver contentResolver, Integer boothId) {

        final CreateShopParams[] temp = new CreateShopParams[1];

        try {
            createAvatarPhotoResponse.setValue(new Event<>(ApiResponse.loading()));
            disposables.add(Observable.just(MediaStore.Images.Media.getBitmap(contentResolver, uri))
                    .subscribeOn(Schedulers.io())
                    .flatMap(bitmap -> {
                        if (!BitmapUtils.checkSizeBitmap(bitmap, 2000000)) {
                            throw new Exception("Bitmap too large. Bitmap must low 2MB");
                        }
                        return Observable.just(BitmapUtils.encodeTobase64(bitmap));
                    })
                    .flatMap(base64 -> {
                        // create photo params
                        CreatePhotoParams params = new CreatePhotoParams();
                        String uuid = UUID.randomUUID().toString();
                        params.setBase64String(base64);
                        params.setName(uuid);
                        params.setAlt(uuid);

                        //TODO: enum for clean code
                        params.setPhotoTypeId(1);
                        params.setStatus(1);

                        return Observable.just(params);
                    })
                    .flatMap(params -> repository.getTaskRemoteDataSource().createPhoto(repository.authToken.token, params))
                    .flatMap(response -> {
                        if (response.isSuccess()) {
                            return Observable.just(response.getData());
                        }
                        throw new Exception(response.getMessage());
                    }).flatMap(createPhoto -> {

                        final BoothItemData[] boothItemData = new BoothItemData[1];
                        List<BoothItemData> listBooth = Objects.requireNonNull(repository.listBoothLiveData.getValue()).getContent();

                        for (int i = 0; i < listBooth.size(); i++) {
                            if (boothId.equals(listBooth.get(i).getId())) {
                                boothItemData[0] = listBooth.get(i);
                            }
                        }

                        if (boothItemData[0] == null) {
                            throw new Exception("Not found any booth that match with booth id in list booth");
                        }

                        CreateShopParams createShopParams = new CreateShopParams();
                        createShopParams.setId(boothItemData[0].getId());
                        createShopParams.setCode(boothItemData[0].getCode());
                        createShopParams.setName(boothItemData[0].getName());
                        createShopParams.setAddress(boothItemData[0].getAddress());
                        createShopParams.setHotline(boothItemData[0].getHotline());
                        createShopParams.setMobile(boothItemData[0].getMobile());
                        createShopParams.setWebsite(boothItemData[0].getWebsite());

                        createShopParams.setAvatar(Urls.HOST + createPhoto.getUrl());
                        createShopParams.setCardImage(boothItemData[0].getCardImage());

                        temp[0] = createShopParams;

                        List<BoothItemData> boothList = repository.listBoothLiveData.getValue().getContent();

                        if (boothList == null) {
                            throw new Exception("Booth list must not null");
                        }


                        int index = U.findIndex(boothList, arg -> boothId.equals(arg.getId()));
                        BoothItemData data = boothList.get(index);

                        data.setCode(temp[0].getCode());
                        data.setName(temp[0].getName());
                        data.setAddress(temp[0].getAddress());
                        data.setWebsite(temp[0].getWebsite());
                        data.setMap(temp[0].getMap());
                        data.setHotline(temp[0].getHotline());
                        data.setMobile(temp[0].getMobile());
                        data.setAvatar(temp[0].getAvatar());
                        data.setCardImage(temp[0].getCardImage());

                        repository.listBoothLiveData.postValue(new Event(boothList));

                        createAvatarPhotoResponse.postValue(new Event<>(ApiResponse.success(uri)));

                        return repository.getTaskRemoteDataSource().updateShop(repository.authToken.token, createShopParams);

                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe((d) ->
                            createCoverPhotoResponse.setValue(new Event<>(ApiResponse.loading())))
                    .subscribe(
                            result -> {

                                if (temp[0] == null) {
                                    throw new Exception("Not found CreateShopParams");
                                }

                                if (result.isSuccess()) {
                                    updateShopResponse.setValue(new Event<>(ApiResponse.success(result)));
                                } else {
                                    updateShopResponse.setValue(new Event<>(ApiResponse.error(new Throwable(result.getMessage()))));
                                }

                            },
                            throwable -> {
                                createAvatarPhotoResponse.setValue(new Event<>(ApiResponse.error(throwable)));
                                updateShopResponse.setValue(new Event<>(ApiResponse.error(throwable)));
                            }
                    ));
        } catch (IOException e) {
            createAvatarPhotoResponse.setValue(new Event<>(ApiResponse.error(e)));
            e.printStackTrace();
        }


    }


}
