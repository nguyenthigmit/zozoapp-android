package com.zozo.imagepicker;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.appbar.MaterialToolbar;
import com.yalantis.ucrop.view.UCropView;
import com.zozo.R;
import com.zozo.ViewModelFactory;
import com.zozo.ZozoApplication;
import com.zozo.constant.KeyConstant;
import com.zozo.event.Event;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.ymex.popup.controller.ProgressController;
import cn.ymex.popup.dialog.PopupDialog;

public class CropImageActivity extends AppCompatActivity {

    public static String AVATAR_TYPE = "avatar";
    public static String COVER_TYPE = "cover";

    @Inject
    ViewModelFactory viewModelFactory;

    CropImageViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crop_image_act);
        ButterKnife.bind(this);
        ((ZozoApplication) getApplication()).getAppComponent().doInjection(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CropImageViewModel.class);
        this.handleIntent();
    }

    public void handleIntent() {

        Bundle bundle = getIntent().getExtras();

        if (bundle == null) {
            finish();
        }

        Uri uri = bundle.getParcelable(KeyConstant.URI);
        String type = bundle.getString(KeyConstant.TYPE);
        Integer boothId = bundle.getInt(KeyConstant.ID);
        String nowAvatarUrl = bundle.getString(KeyConstant.URL);

        if (uri == null || type == null || boothId == null) {
            finish();
        }

        if (type.equals(AVATAR_TYPE)) {
            this.replaceCropAvatarFragment(uri, boothId);
        } else {
            this.replaceCropCoverFragment(uri, boothId, nowAvatarUrl);
        }


    }

    public void replaceCropAvatarFragment(Uri uri, Integer boothId) {
        getSupportFragmentManager().beginTransaction().replace(R.id.crop_image_container, CropAvatarImageFragment.newInstance(uri, boothId)).commit();
    }

    public void replaceCropCoverFragment(Uri uri, Integer boothId, String nowAvatarUrl) {
        getSupportFragmentManager().beginTransaction().replace(R.id.crop_image_container, CropCoverImageFragment.newInstance(uri, boothId, nowAvatarUrl)).commit();
    }




}
