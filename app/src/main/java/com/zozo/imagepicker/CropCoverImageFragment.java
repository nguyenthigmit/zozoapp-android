package com.zozo.imagepicker;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.fenchtose.nocropper.BitmapCropCallback;
import com.fenchtose.nocropper.CropperImageView;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.squareup.picasso.Picasso;
import com.zozo.R;
import com.zozo.constant.KeyConstant;
import com.zozo.event.Event;
import com.zozo.utils.ApiResponse;
import com.zozo.utils.BitmapUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.ymex.popup.controller.ProgressController;
import cn.ymex.popup.dialog.PopupDialog;
import de.hdodenhof.circleimageview.CircleImageView;

public class CropCoverImageFragment extends Fragment implements Toolbar.OnMenuItemClickListener {


    @BindView(R.id.toolbar)
    MaterialToolbar toolbar;

    @BindView(R.id.cover_cropper_image)
    CropperImageView cropperImageView;

    @BindView(R.id.avatar_image)
    CircleImageView avatarImage;

    CropImageViewModel viewModel;

    PopupDialog popupDialog;


    public static CropCoverImageFragment newInstance(Uri uri, Integer boothId, String nowAvatarUrl) {
        CropCoverImageFragment cropCoverImageFragment = new CropCoverImageFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KeyConstant.URI, uri);
        bundle.putInt(KeyConstant.ID, boothId);
        bundle.putString(KeyConstant.URL, nowAvatarUrl);

        cropCoverImageFragment.setArguments(bundle);
        return cropCoverImageFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.crop_cover_image_fm, container, false);
        ButterKnife.bind(this, view);
        viewModel = ((CropImageActivity) getActivity()).viewModel;
        viewModel.createCoverPhotoResponse.observe(this, this::handleCreatePhto);
        popupDialog = PopupDialog.create(this);
        toolbar.setNavigationOnClickListener(v -> getActivity().finish());
        toolbar.inflateMenu(R.menu.image_crop_menu);
        toolbar.setOnMenuItemClickListener(this);
        this.handleArgument();
        return view;
    }

    public void handleArgument() {
        Uri uri = getArguments().getParcelable(KeyConstant.URI);
        String nowAvatarUrl = getArguments().getString(KeyConstant.URL);

        if (nowAvatarUrl != null && !nowAvatarUrl.equals("")) {
            Picasso.get().load(nowAvatarUrl).error(R.drawable.default_avatar_shop).into(avatarImage);
        } else {
            Picasso.get().load(R.drawable.default_avatar_shop).into(avatarImage);
        }

        if (uri != null) {
            this.setupCoverType(uri);
        }

    }

    public void setupCoverType(Uri uri) {
        String filePath = BitmapUtils.getFilePathFromUri(getContext(), uri);
        Bitmap mBitmap = BitmapFactory.decodeFile(filePath);

        int maxP = Math.max(mBitmap.getWidth(), mBitmap.getHeight());
        float scale1280 = (float) maxP / 1280;

        if (cropperImageView.getWidth() != 0) {
            cropperImageView.setMaxZoom(cropperImageView.getWidth() * 2 / 1280f);
        } else {

            ViewTreeObserver vto = cropperImageView.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    cropperImageView.getViewTreeObserver().removeOnPreDrawListener(this);
                    cropperImageView.setMaxZoom(cropperImageView.getWidth() * 2 / 1280f);
                    return true;
                }
            });

        }

        mBitmap = Bitmap.createScaledBitmap(mBitmap, (int) (mBitmap.getWidth() / scale1280),
                (int) (mBitmap.getHeight() / scale1280), true);

        cropperImageView.setImageBitmap(mBitmap);
        cropperImageView.setHasZoom(false);
        cropperImageView.setMakeSquare(false);
        cropperImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_image_crop:

                PackageManager m = getContext().getPackageManager();
                String s = getContext().getPackageName();
                PackageInfo p = null;
                try {
                    p = m.getPackageInfo(s, 0);
                    s = "file://" + p.applicationInfo.dataDir + "/crop_image.png";
                    cropperImageView.cropBitmapAsync(getContext(), new URL(s), new BitmapCropCallback() {
                        @Override
                        public void onBitmapCropped(@NonNull URL resultUrl, int byteCount) {
                            Integer id = getArguments().getInt(KeyConstant.ID, 0);

                            // create photo sever
                            viewModel.createPhotoCover(id, getActivity().getCacheDir(), resultUrl, byteCount);

                        }

                        @Override
                        public void onCropFailure(@NonNull Throwable t) {

                        }
                    });

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                return true;
        }

        return false;
    }

    public void cropImageSuccess(URL resultUrl, int byteCount) {
        Intent intent = new Intent();
        intent.putExtra(KeyConstant.URL, resultUrl);
        intent.putExtra(KeyConstant.COUNT, byteCount);
        getActivity().setResult(getActivity().RESULT_OK, intent);
        getActivity().finish();
    }

    public void handleCreatePhto(Event<ApiResponse> event) {
        ApiResponse apiResponse = event.getContentIfNotHandled();

        if (apiResponse.status == null) {
            return;
        }

        switch (apiResponse.status) {
            case LOADING:
                popupDialog.setBackgroundDrawable(getResources().getDrawable(R.drawable.error_rounded_edittext));
                popupDialog.controller(ProgressController.build().message(getResources().getString(R.string.loading)))
                        .backgroundDrawable(new ColorDrawable(Color.parseColor("#66000000")))
                        .show();
                break;
            case SUCCESS:
                popupDialog.dismiss();
                Map<String, Object> data = (Map<String, Object>) apiResponse.data;
                CropCoverImageFragment.this.cropImageSuccess((URL) data.get("url"), (int) data.get("byteCount"));
                break;
            case ERROR:
                popupDialog.dismiss();
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
                builder.setMessage(Objects.requireNonNull(apiResponse.error).getMessage());
                builder.setNegativeButton(getResources().getText(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
                break;
        }

    }
}
