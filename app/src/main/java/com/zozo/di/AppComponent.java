package com.zozo.di;

import com.zozo.screens.booth.BoothActivity;
import com.zozo.screens.booth.detail.BoothDetailActivity;
import com.zozo.screens.booth.product.CreateProductActivity;
import com.zozo.screens.forgotpassword.ForgotPasswordActivity;
import com.zozo.screens.home.HomeActivity;
import com.zozo.imagepicker.CropImageActivity;
import com.zozo.screens.launch.LaunchActivity;
import com.zozo.screens.login.LoginActivity;
import com.zozo.screens.register.RegisterActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ServiceModule.class})
public interface AppComponent {

    void doInjection(LoginActivity loginActivity);
    void doInjection(LaunchActivity launchActivity);
    void doInjection(ForgotPasswordActivity forgotPasswordActivity);
    void doInjection(RegisterActivity registerActivity);
    void doInjection(HomeActivity homeActivity);
    void doInjection(BoothActivity boothActivity);
    void doInjection(BoothDetailActivity boothDetailActivity);
    void doInjection(CropImageActivity cropImageActivity);
    void doInjection(CreateProductActivity createProductActivity);

}
