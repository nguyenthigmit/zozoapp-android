package com.zozo.di;

import android.content.Context;

import androidx.lifecycle.ViewModelProvider;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.zozo.ViewModelFactory;
import com.zozo.source.Repository;
import com.zozo.source.local.TaskLocalDataSource;
import com.zozo.source.remote.TaskRemoteDataSource;
import com.zozo.source.remote.ZozoAPI;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.zozo.constant.Urls.BASE_URL;

@Module
public class ServiceModule {

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder builder = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return builder.setLenient().create();
    }


    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Provides
    @Singleton
    ZozoAPI getApiCallInterface(Retrofit retrofit) {
        return retrofit.create(ZozoAPI.class);
    }

    @Provides
    @Singleton
    OkHttpClient getRequestHeader() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder requestBuilder = chain.request().newBuilder();
                requestBuilder.header("Content-Type", "application/json");
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        })
                .connectTimeout(100, TimeUnit.SECONDS)
                .writeTimeout(100, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS);

        return httpClient.build();
    }

    @Provides
    @Singleton
    TaskRemoteDataSource provideTaskRemoteDataSource(ZozoAPI zozoAPI) {
        return TaskRemoteDataSource.getInstance(zozoAPI);
    }

    @Provides
    @Singleton
    TaskLocalDataSource provideTaskLocalDataSource() {
        return TaskLocalDataSource.getInstance();
    }

    @Provides
    @Singleton
    Repository getRepository(TaskRemoteDataSource taskRemoteDataSource, TaskLocalDataSource taskLocalDataSource, Context context) {
        return new Repository(taskRemoteDataSource, taskLocalDataSource, context);
    }

    @Provides
    @Singleton
    ViewModelProvider.Factory getViewModelFactory(Repository repository) {
        return new ViewModelFactory(repository);
    }

}
