package com.zozo.constant;

public class KeyConstant {

    public static String BUNDLE = "bundle";
    public static String ID = "id";
    public static String URI = "uri";
    public static String URL = "url";
    public static String TYPE = "type";
    public static String BITMAP = "bitmap";
    public static String BYTE_ARRAY = "byte_array";
    public static String COUNT = "count";
    public static String ACTIVITY_NAME_BUNDLE_ID = "ACTIVITY_NAME_BUNDLE_ID";
    public static String USERNAME = "USERNAME";

    public static int CROP_IMAGE_AVATAR_REQUEST_CODE = 0x001;
    public static int CROP_IMAGE_COVER_REQUEST_CODE = 0x002;
    public static int PICK_IMAGE_AVATAR_REQUEST_CODE = 0x003;
    public static int PICK_IMAGE_COVER_REQUEST_CODE = 0x004;

    public static String ACTION_VERIFY_ACCOUNT = "ACTION_VERIFY_ACCOUNT";

}
