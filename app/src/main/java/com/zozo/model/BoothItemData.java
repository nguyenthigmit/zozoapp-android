package com.zozo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoothItemData {

    @SerializedName("Id")
    @Expose
    private Integer id;

    @SerializedName("Code")
    @Expose
    private String code;

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("Address")
    @Expose
    private String address;

    @SerializedName("Mobile")
    @Expose
    private String mobile;

    @SerializedName("Hotline")
    @Expose
    private String hotline;

    @SerializedName("CardImage")
    @Expose
    private String cardImage;

    @SerializedName("Avatar")
    @Expose
    private String avatar;

    @SerializedName("map")
    @Expose
    private String map;

    @SerializedName("Website")
    @Expose
    private String website;

    @SerializedName("CountOrdered")
    @Expose
    private Integer countOrdered;

    @SerializedName("TotalNewOrders")
    @Expose
    private Integer totalNewOrders;

    @SerializedName("TotalFollows")
    @Expose
    private Integer totalFollows;

    public BoothItemData() {

    }

    public BoothItemData(CreateShopParams boothItemData) {
        this.code = boothItemData.getCode();
        this.name = boothItemData.getName();
        this.address = boothItemData.getAddress();
        this.website = boothItemData.getWebsite();
        this.map = boothItemData.getMap();
        this.hotline = boothItemData.getHotline();
        this.mobile = boothItemData.getMobile();
        this.avatar = boothItemData.getAvatar();
        this.cardImage = boothItemData.getCardImage();
    }


    public Integer getCountOrdered() {
        return countOrdered;
    }

    public void setCountOrdered(Integer countOrdered) {
        this.countOrdered = countOrdered;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getHotline() {
        return hotline;
    }

    public void setHotline(String hotline) {
        this.hotline = hotline;
    }

    public String getCardImage() {
        return cardImage;
    }

    public void setCardImage(String cardImage) {
        this.cardImage = cardImage;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Integer getTotalNewOrders() {
        return totalNewOrders;
    }

    public void setTotalNewOrders(Integer totalNewOrders) {
        this.totalNewOrders = totalNewOrders;
    }

    public Integer getTotalFollows() {
        return totalFollows;
    }

    public void setTotalFollows(Integer totalFollows) {
        this.totalFollows = totalFollows;
    }
}
