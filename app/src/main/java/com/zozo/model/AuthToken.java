package com.zozo.model;

public class AuthToken {

    public String token;
    public String userId;

    public AuthToken() {

    }

    public AuthToken(String token, String userId) {
        this.token = token;
        this.userId = userId;
    }

}
