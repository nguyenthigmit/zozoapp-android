package com.zozo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginParams {

    @SerializedName("UserName")
    @Expose
    public String username;

    @SerializedName("Password")
    @Expose
    public String password;

    public LoginParams(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
