package com.zozo.model;

public enum UserType {

    Customer("KH");

    String value;

    UserType(String value) {
        this.value = value;
    }

    public  String getValue() {
        return this.value;
    }
}

