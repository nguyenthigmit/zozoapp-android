package com.zozo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreatePhotoResponse {

    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;

    @SerializedName("UpdatedOn")
    @Expose
    private Object updatedOn;

    @SerializedName("CreatedBy")
    @Expose
    private String createdBy;

    @SerializedName("UpdatedBy")
    @Expose
    private String updatedBy;

    @SerializedName("Url")
    @Expose
    private String url;

    @SerializedName("PhotoTypeName")
    @Expose
    private Integer photoTypeName;

    @SerializedName("Id")
    @Expose
    private Integer id;

    @SerializedName("Order")
    @Expose
    private Integer order;

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("Alt")
    @Expose
    private String alt;

    @SerializedName("Note")
    @Expose
    private String note;

    @SerializedName("PhotoTypeId")
    @Expose
    private Integer photoTypeId;

    @SerializedName("Base64String")
    @Expose
    private String base64String;

    @SerializedName("Status")
    @Expose
    private Integer status;

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Object getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Object updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Object getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Object getPhotoTypeName() {
        return photoTypeName;
    }

    public void setPhotoTypeName(Integer photoTypeName) {
        this.photoTypeName = photoTypeName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getPhotoTypeId() {
        return photoTypeId;
    }

    public void setPhotoTypeId(Integer photoTypeId) {
        this.photoTypeId = photoTypeId;
    }

    public String getBase64String() {
        return base64String;
    }

    public void setBase64String(String base64String) {
        this.base64String = base64String;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
