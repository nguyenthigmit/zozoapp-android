package com.zozo.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseModel<T> {

    @SerializedName("message")
    @Expose
    private String message;


    @SerializedName("data")
    @Expose
    private T data;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("errorNumber")
    @Expose
    private Object errorNumber;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getErrorNumber() {
        return errorNumber;
    }

    public void setErrorNumber(Object errorNumber) {
        this.errorNumber = errorNumber;
    }

    @NonNull
    @Override
    public String toString() {
        return "success " + this.success + ", message: " + this.message + ", data: " + this.data;
    }
}
