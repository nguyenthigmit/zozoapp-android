package com.zozo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User {

    @SerializedName("UserId")
    @Expose
    private String userId;

    @SerializedName("Email")
    @Expose
    private String email;

    @SerializedName("UserName")
    @Expose
    private String userName;

    @SerializedName("FirstName")
    @Expose
    private String firstName;

    @SerializedName("LastName")
    @Expose
    private String lastName;

    @SerializedName("Birthday")
    @Expose
    private String birthday;

    @SerializedName("Gender")
    @Expose
    private Object gender;

    @SerializedName("FacebookId")
    @Expose
    private Object facebookId;

    @SerializedName("PictureUrl")
    @Expose
    private Object pictureUrl;

    @SerializedName("PhoneNumber")
    @Expose
    private Object phoneNumber;

    @SerializedName("UserType")
    @Expose
    private String userType;

    @SerializedName("FacebookAccount")
    @Expose
    private Object facebookAccount;

    @SerializedName("ZaloAccount")
    @Expose
    private Object zaloAccount;

    @SerializedName("IsMarried")
    @Expose
    private Object isMarried;

    @SerializedName("RefName")
    @Expose
    private Object refName;

    @SerializedName("Introduce")
    @Expose
    private Object introduce;

    @SerializedName("Career")
    @Expose
    private Object career;

    @SerializedName("CardImage")
    @Expose
    private Object cardImage;

    @SerializedName("ShopId")
    @Expose
    private Object shopId;

    @SerializedName("Status")
    @Expose
    private Integer status;

    @SerializedName("Roles")
    @Expose
    private List<String> roles = null;

    @SerializedName("Latitude")
    @Expose
    private String latitude;

    @SerializedName("Longitude")
    @Expose
    private String longitude;

    @SerializedName("Address")
    @Expose
    private String address;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public Object getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    public Object getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(Object pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Object getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Object phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Object getFacebookAccount() {
        return facebookAccount;
    }

    public void setFacebookAccount(Object facebookAccount) {
        this.facebookAccount = facebookAccount;
    }

    public Object getZaloAccount() {
        return zaloAccount;
    }

    public void setZaloAccount(Object zaloAccount) {
        this.zaloAccount = zaloAccount;
    }

    public Object getIsMarried() {
        return isMarried;
    }

    public void setIsMarried(Object isMarried) {
        this.isMarried = isMarried;
    }

    public Object getRefName() {
        return refName;
    }

    public void setRefName(Object refName) {
        this.refName = refName;
    }

    public Object getIntroduce() {
        return introduce;
    }

    public void setIntroduce(Object introduce) {
        this.introduce = introduce;
    }

    public Object getCareer() {
        return career;
    }

    public void setCareer(Object career) {
        this.career = career;
    }

    public Object getCardImage() {
        return cardImage;
    }

    public void setCardImage(Object cardImage) {
        this.cardImage = cardImage;
    }

    public Object getShopId() {
        return shopId;
    }

    public void setShopId(Object shopId) {
        this.shopId = shopId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
