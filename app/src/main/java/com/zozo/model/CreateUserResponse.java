package com.zozo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class CreateUserResponse {

    @SerializedName("UserId")
    @Expose
    private String userId;

    @SerializedName("SendMail")
    @Expose
    private Map<String, String> sendMail;

    @SerializedName("SendOTP")
    @Expose
    private Map<String, String> sendOTP;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Map<String, String> getSendMail() {
        return sendMail;
    }

    public void setSendMail(Map<String, String> sendMail) {
        this.sendMail = sendMail;
    }

    public Map<String, String> getSendOTP() {
        return sendOTP;
    }

    public void setSendOTP(Map<String, String> sendOTP) {
        this.sendOTP = sendOTP;
    }
}
