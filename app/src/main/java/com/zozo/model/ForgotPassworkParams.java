package com.zozo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class ForgotPassworkParams {

    @SerializedName("UserId")
    @Expose
    private String userId;

    @SerializedName("SendMail")
    @Expose
    private Map<String, Object> sendMail;

    @SerializedName("SendOTP")
    @Expose
    private Map<String, Object> sendOTP;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Map<String, Object> getSendMail() {
        return sendMail;
    }

    public void setSendMail(Map<String, Object> sendMail) {
        this.sendMail = sendMail;
    }

    public Map<String, Object> getSendOTP() {
        return sendOTP;
    }

    public void setSendOTP(Map<String, Object> sendOTP) {
        this.sendOTP = sendOTP;
    }
}
