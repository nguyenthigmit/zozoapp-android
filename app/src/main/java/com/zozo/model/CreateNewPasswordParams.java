package com.zozo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateNewPasswordParams {

    @SerializedName("NewPassword")
    @Expose
    private String newPassword;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("Code")
    @Expose
    private String code;

    public CreateNewPasswordParams() {

    }

    public CreateNewPasswordParams(String newPassword, String userId, String code) {
        this.code = code;
        this.newPassword = newPassword;
        this.userId = userId;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
