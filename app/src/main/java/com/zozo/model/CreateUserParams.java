package com.zozo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateUserParams {


    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("PasswordConfirm")
    @Expose
    private String passwordConfirm;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Birthday")
    @Expose
    private String birthday;
    @SerializedName("Gender")
    @Expose
    private Boolean gender;
    @SerializedName("FacebookId")
    @Expose
    private Integer facebookId;
    @SerializedName("PictureUrl")
    @Expose
    private Object pictureUrl;
    @SerializedName("PhoneNumber")
    @Expose
    private Object phoneNumber;
    @SerializedName("UserType")
    @Expose
    private String userType;
    @SerializedName("FacebookAccount")
    @Expose
    private Object facebookAccount;
    @SerializedName("ZaloAccount")
    @Expose
    private Object zaloAccount;
    @SerializedName("IsMarried")
    @Expose
    private Boolean isMarried;
    @SerializedName("RefName")
    @Expose
    private Object refName;
    @SerializedName("Introduce")
    @Expose
    private Object introduce;
    @SerializedName("Career")
    @Expose
    private Object career;
    @SerializedName("CardImage")
    @Expose
    private Object cardImage;
    @SerializedName("ShopId")
    @Expose
    private Object shopId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public Integer getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(Integer facebookId) {
        this.facebookId = facebookId;
    }

    public Object getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(Object pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Object getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Object phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Object getFacebookAccount() {
        return facebookAccount;
    }

    public void setFacebookAccount(Object facebookAccount) {
        this.facebookAccount = facebookAccount;
    }

    public Object getZaloAccount() {
        return zaloAccount;
    }

    public void setZaloAccount(Object zaloAccount) {
        this.zaloAccount = zaloAccount;
    }

    public Boolean getIsMarried() {
        return isMarried;
    }

    public void setIsMarried(Boolean isMarried) {
        this.isMarried = isMarried;
    }

    public Object getRefName() {
        return refName;
    }

    public void setRefName(Object refName) {
        this.refName = refName;
    }

    public Object getIntroduce() {
        return introduce;
    }

    public void setIntroduce(Object introduce) {
        this.introduce = introduce;
    }

    public Object getCareer() {
        return career;
    }

    public void setCareer(Object career) {
        this.career = career;
    }

    public Object getCardImage() {
        return cardImage;
    }

    public void setCardImage(Object cardImage) {
        this.cardImage = cardImage;
    }

    public Object getShopId() {
        return shopId;
    }

    public void setShopId(Object shopId) {
        this.shopId = shopId;
    }

}
