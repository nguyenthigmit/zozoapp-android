package com.zozo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckValidCodeParams {

    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("Code")
    @Expose
    private String code;

    public CheckValidCodeParams() {

    }

    public CheckValidCodeParams(String userId, String code) {
        this.userId = userId;
        this.code = code;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


}
