package com.zozo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreatePhotoParams {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Alt")
    @Expose
    private String alt;

    @SerializedName("Note")
    @Expose
    private String note;

    @SerializedName("PhotoTypeId")
    @Expose
    private int photoTypeId;

    @SerializedName("Base64String")
    @Expose
    private String base64String;

    @SerializedName("Status")
    @Expose
    private int status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getPhotoTypeId() {
        return photoTypeId;
    }

    public void setPhotoTypeId(int photoTypeId) {
        this.photoTypeId = photoTypeId;
    }

    public String getBase64String() {
        return base64String;
    }

    public void setBase64String(String base64String) {
        this.base64String = base64String;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


}

