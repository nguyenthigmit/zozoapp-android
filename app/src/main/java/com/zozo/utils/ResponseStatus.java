package com.zozo.utils;

public enum ResponseStatus {
    LOADING,
    SUCCESS,
    ERROR
}
