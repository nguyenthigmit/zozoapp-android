package com.zozo.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static com.zozo.utils.ResponseStatus.ERROR;
import static com.zozo.utils.ResponseStatus.LOADING;
import static com.zozo.utils.ResponseStatus.SUCCESS;


/**
 * Created by ${trungle} on 03-05-2018.
 */

public class ApiResponse {

    public final ResponseStatus status;

    @Nullable
    public final Object data;

    @Nullable
    public final Throwable error;

    private ApiResponse(ResponseStatus status, @Nullable Object data, @Nullable Throwable error) {
        this.status = status;
        this.data = data;
        this.error = error;
    }

    public static ApiResponse loading() {
        return new ApiResponse(LOADING, null, null);
    }

    public static ApiResponse success(@NonNull Object data) {
        return new ApiResponse(SUCCESS, data, null);
    }

    public static ApiResponse error(@NonNull Throwable error) {
        return new ApiResponse(ERROR, null, error);
    }

}
