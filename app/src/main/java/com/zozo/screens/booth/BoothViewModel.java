package com.zozo.screens.booth;

import android.text.Editable;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.zozo.event.Event;
import com.zozo.model.BoothItemData;
import com.zozo.model.CreateShopParams;
import com.zozo.source.Repository;
import com.zozo.utils.ApiResponse;
import com.zozo.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BoothViewModel extends ViewModel {

    public final MutableLiveData<String> boothCode = new MutableLiveData<>();
    public final MutableLiveData<String> boothName = new MutableLiveData<>();
    public final MutableLiveData<String> boothNumberPhone = new MutableLiveData<>();
    public final MutableLiveData<String> boothAddress = new MutableLiveData<>();
    public final MutableLiveData<String> boothHotline = new MutableLiveData<>();
    public final MutableLiveData<String> boothLinkGoogle = new MutableLiveData<>();
    public final MutableLiveData<String> boothLinkWebsite = new MutableLiveData<>();
    public final MutableLiveData<Boolean> disabledCreated = new MutableLiveData<>();

    public final MutableLiveData<Event<ApiResponse>> responseGetAllShop = new MutableLiveData<>();
    public final MutableLiveData<Event<ApiResponse>> responseCreateShop = new MutableLiveData<>();
    public MutableLiveData<Event<List<BoothItemData>>> boothList;


    private Repository repository;
    private final CompositeDisposable disposables = new CompositeDisposable();

    public BoothViewModel(Repository repository) {
        this.repository = repository;
        this.disabledCreated.setValue(true);
        boothList = repository.listBoothLiveData;
    }

    public void getAllShopByUserId() {

        disposables.add(repository.getTaskRemoteDataSource()
                .getShopByUserId(repository.authToken.token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).doOnSubscribe((d) ->
                        responseGetAllShop.setValue(new Event<>(ApiResponse.loading())))
                .subscribe(
                        result -> {
                            if (result.isSuccess()) {
                                responseGetAllShop.setValue(new Event<>(ApiResponse.success(result.getData())));
                                repository.listBoothLiveData.setValue(new Event<>(result.getData()));
                                boothList.setValue(new Event(result.getData()));
                            } else {
                                responseGetAllShop.setValue(new Event<>(ApiResponse.error(new Throwable(result.getMessage()))));
                            }

                        },
                        throwable -> responseGetAllShop.setValue(new Event<>(ApiResponse.error(throwable)))
                )

        );
    }

    public void resetCacheCreate() {
        this.boothCode.setValue("");
        this.boothName.setValue("");
        this.boothNumberPhone.setValue("");
        this.boothHotline.setValue("");
        this.boothLinkWebsite.setValue("");
        this.boothLinkGoogle.setValue("");
        this.boothAddress.setValue("");
    }

    public void createShop() {

        CreateShopParams params = new CreateShopParams();
        params.setCode(this.boothCode.getValue());
        params.setName(this.boothName.getValue());
        params.setAddress(this.boothAddress.getValue());
        params.setMobile(this.boothNumberPhone.getValue());
        params.setHotline(this.boothHotline.getValue());
        params.setWebsite(this.boothLinkWebsite.getValue());
        params.setMap(this.boothLinkGoogle.getValue());

        disposables.add(repository.getTaskRemoteDataSource()
                .createShop(repository.authToken.token, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).doOnSubscribe((d) ->
                        responseCreateShop.setValue(new Event<>(ApiResponse.loading())))
                .subscribe(
                        result -> {
                            if (result.isSuccess()) {

                                BoothItemData boothItemData = new BoothItemData();
                                boothItemData.setAddress(params.getAddress());
                                boothItemData.setName(params.getName());
                                boothItemData.setCode(params.getCode());
                                boothItemData.setMobile(params.getMobile());
                                boothItemData.setHotline(params.getHotline());
                                boothItemData.setAvatar(null);
                                boothItemData.setCardImage(null);
                                boothItemData.setMap(params.getMap());
                                boothItemData.setWebsite(params.getWebsite());
                                boothItemData.setTotalFollows(0);
                                boothItemData.setCountOrdered(0);
                                boothItemData.setId(Integer.parseInt(result.getData()));
                                responseCreateShop.setValue(new Event<>(ApiResponse.success(boothItemData)));

                                List<BoothItemData> newBoothList = new ArrayList<>(this.boothList.getValue().getContent());
                                newBoothList.add(0, boothItemData);

                                repository.listBoothLiveData.setValue(new Event<>(newBoothList));
                                boothList.setValue(new Event(newBoothList));

                                this.resetCacheCreate();

                            } else {
                                responseCreateShop.setValue(new Event<>(ApiResponse.error(new Throwable(result.getMessage()))));
                            }
                        },
                        throwable -> responseCreateShop.setValue(new Event<>(ApiResponse.error(throwable)))
                )

        );
    }

    public void afterChangedTextName(Editable s) {
        this.boothCode.setValue(StringUtils.createShopCode(s.toString()));
        this.afterChangedText(s);
    }

    public void afterChangedText(Editable s) {

        if (boothCode.getValue() == null || boothCode.getValue().trim().equals("")) {
            this.disabledCreated.setValue(true);
            return;
        }

        if (boothName.getValue() == null || boothName.getValue().trim().equals("")) {
            this.disabledCreated.setValue(true);
            return;
        }


        if (boothNumberPhone.getValue() == null || boothNumberPhone.getValue().trim().equals("")) {
            this.disabledCreated.setValue(true);
            return;
        }

        if (boothAddress.getValue() == null || boothAddress.getValue().trim().equals("")) {
            this.disabledCreated.setValue(true);
            return;
        }

        if (boothHotline.getValue() == null || boothHotline.getValue().trim().equals("")) {
            this.disabledCreated.setValue(true);
            return;
        }

        if (boothLinkGoogle.getValue() == null || boothLinkGoogle.getValue().trim().equals("")) {
            this.disabledCreated.setValue(true);
            return;
        }

        if (boothLinkWebsite.getValue() == null || boothLinkWebsite.getValue().trim().equals("")) {
            this.disabledCreated.setValue(true);
            return;
        }


        this.disabledCreated.setValue(false);
    }

    public BoothItemData getBoothItemData(int id) {

        List<BoothItemData> boothList = this.repository.listBoothLiveData.getValue().getContent();
        BoothItemData data = new BoothItemData();

        for (int i = 0; i < boothList.size(); i++) {
            if (boothList.get(i).getId().equals(id)) {
                data = boothList.get(i);
                break;
            }
        }

        return data;
    }


}
