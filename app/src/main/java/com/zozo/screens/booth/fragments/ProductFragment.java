package com.zozo.screens.booth.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zozo.R;
import com.zozo.screens.booth.product.CreateProductActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductFragment extends Fragment {

    public static ProductFragment newInstance() {
        ProductFragment productFragment = new ProductFragment();
        Bundle bundle = new Bundle();
        productFragment.setArguments(bundle);

        return productFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.booth_detail_product_fm, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.create_product_linear)
    public void createProduct() {
        Intent intent = new Intent(getContext(), CreateProductActivity.class);
        startActivity(intent);
    }
}
