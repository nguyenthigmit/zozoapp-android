package com.zozo.screens.booth.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.zozo.R;
import com.zozo.screens.booth.fragments.GalleryFragment;
import com.zozo.screens.booth.fragments.MessageFragment;
import com.zozo.screens.booth.fragments.OrderFragment;
import com.zozo.screens.booth.fragments.ProductFragment;
import com.zozo.screens.booth.fragments.ShopFragment;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

public class BoothDetailAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;
    private WeakReference<Context> contextWeakReference;

    public BoothDetailAdapter(@NonNull FragmentManager fm, Context context, List<Fragment> fragments) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

        this.fragments = fragments;
        this.contextWeakReference = new WeakReference<>(context);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments != null ? fragments.size() : 0;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        if(this.contextWeakReference.get() == null) {
            return super.getPageTitle(position);
        }

        Fragment fragment = this.fragments.get(position);
        Context context = this.contextWeakReference.get();

        if (fragment instanceof ProductFragment) {
            return Objects.requireNonNull(context).getString(R.string.product);
        } else if (fragment instanceof ShopFragment) {
            return Objects.requireNonNull(context).getString(R.string.shop);
        } else if (fragment instanceof GalleryFragment) {
            return Objects.requireNonNull(context).getString(R.string.gallery_photo);
        } else if (fragment instanceof MessageFragment) {
            return Objects.requireNonNull(context).getString(R.string.message);
        } else if (fragment instanceof OrderFragment) {
            return Objects.requireNonNull(context).getString(R.string.order);
        }

        return super.getPageTitle(position);
    }
}