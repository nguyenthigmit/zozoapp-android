package com.zozo.screens.booth.product;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.google.android.material.snackbar.Snackbar;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;
import com.zozo.R;
import com.zozo.ViewModelFactory;
import com.zozo.ZozoApplication;
import com.zozo.screens.booth.adapter.KeywordAdapter;
import com.zozo.databinding.CreateProductActBinding;
import com.zozo.view.recyclerview.VerticalAndHorizontalSpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.zozo.utils.ActivityUtils.setupHidenKeyboarTouchOutside;

public class CreateProductActivity extends AppCompatActivity {

    @Inject
    ViewModelFactory factory;

    @BindView(R.id.color_recyclerview)
    RecyclerView colorRecyclerView;

    @BindView(R.id.size_recyclerview)
    RecyclerView sizeRecyclerView;

    @BindView(R.id.spinner)
    MaterialBetterSpinner spinner;

    ProductBoothViewModel viewModel;

    KeywordAdapter colorAdapter;
    KeywordAdapter sizeAdapter;

    List<String> colorKeywordList;
    List<String> sizeKeywordList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CreateProductActBinding binding = DataBindingUtil.setContentView(this, R.layout.create_product_act);

        ButterKnife.bind(this);
        setupHidenKeyboarTouchOutside(this, findViewById(R.id.create_product_container));

        ((ZozoApplication) getApplication()).getAppComponent().doInjection(this);
        viewModel = ViewModelProviders.of(this, factory).get(ProductBoothViewModel.class);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        colorKeywordList = new ArrayList<>();
        colorAdapter = new KeywordAdapter(colorKeywordList);
        colorRecyclerView.setAdapter(colorAdapter);
        colorRecyclerView.setLayoutManager(ChipsLayoutManager.newBuilder(this).build());
        colorRecyclerView.addItemDecoration(new VerticalAndHorizontalSpaceItemDecoration(20, 20));

        sizeKeywordList = new ArrayList<>();
        sizeAdapter = new KeywordAdapter(sizeKeywordList);
        sizeRecyclerView.setAdapter(sizeAdapter);
        sizeRecyclerView.setLayoutManager(ChipsLayoutManager.newBuilder(this).build());
        sizeRecyclerView.addItemDecoration(new VerticalAndHorizontalSpaceItemDecoration(20, 20));
    }

    @OnClick(R.id.add_color_btn)
    public void addColorKeyword() {

        if (viewModel.colorProduct.getValue() == null) {
            return;
        }

        if (colorAdapter.checkDuplicate(viewModel.colorProduct.getValue().trim())) {
            Snackbar.make(findViewById(R.id.create_product_container), String.format(getString(R.string.input_duplication), viewModel.colorProduct.getValue(), getString(R.string.field_color)), 3000).show();
            return;
        }

        colorAdapter.add(viewModel.colorProduct.getValue().trim());
        viewModel.colorProduct.setValue("");
    }

    @OnClick(R.id.add_size_btn)
    public void addSizeKeyword() {

        if (viewModel.sizeProduct.getValue() == null) {
            return;
        }

        if (sizeAdapter.checkDuplicate(viewModel.sizeProduct.getValue().trim())) {
            Snackbar.make(findViewById(R.id.create_product_container), String.format(getString(R.string.input_duplication), viewModel.sizeProduct.getValue(), getString(R.string.field_size)), 3000).show();
            return;
        }

        sizeAdapter.add(viewModel.sizeProduct.getValue().trim());
        viewModel.sizeProduct.setValue("");
    }
}
