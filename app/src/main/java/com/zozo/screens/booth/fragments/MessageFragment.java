package com.zozo.screens.booth.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

public class MessageFragment extends Fragment {

    public static MessageFragment newInstance() {
        MessageFragment messageFragment = new MessageFragment();
        Bundle bundle = new Bundle();
        messageFragment.setArguments(bundle);

        return messageFragment;
    }

}
