package com.zozo.screens.booth.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

public class OrderFragment extends Fragment {

    public static OrderFragment newInstance() {
        OrderFragment orderFragment = new OrderFragment();
        Bundle bundle = new Bundle();
        orderFragment.setArguments(bundle);

        return orderFragment;
    }

}
