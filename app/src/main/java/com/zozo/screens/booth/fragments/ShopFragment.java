package com.zozo.screens.booth.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

public class ShopFragment extends Fragment {

    public static ShopFragment newInstance() {
        ShopFragment shopFragment = new ShopFragment();
        Bundle bundle = new Bundle();
        shopFragment.setArguments(bundle);

        return shopFragment;
    }

}
