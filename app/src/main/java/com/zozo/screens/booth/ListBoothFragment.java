package com.zozo.screens.booth;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.zozo.R;
import com.zozo.screens.booth.detail.BoothDetailActivity;
import com.zozo.event.Event;
import com.zozo.model.BoothItemData;
import com.zozo.utils.ApiResponse;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListBoothFragment extends Fragment implements TaskBooth {

    BoothViewModel viewModel;
    BoothRecycleViewAdapter adapter;

    @BindView(R.id.toolbar)
    MaterialToolbar toolbar;

    @BindView(R.id.booth_list_recycler_view)
    RecyclerView boothRecyclerView;

    @BindView(R.id.shimer_frame)
    ShimmerFrameLayout shimmerFrameLayout;

    @BindView(R.id.load_list_fail)
    LinearLayout loadListFail;

    BottomSheetDialog bottomSheetDialog;


    public static ListBoothFragment newInstance() {
        ListBoothFragment listBoothFragment = new ListBoothFragment();
        Bundle args = new Bundle();
        listBoothFragment.setArguments(args);

        return listBoothFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_booth_fm, container, false);

        ButterKnife.bind(this, view);
        viewModel = ((BoothActivity) Objects.requireNonNull(getActivity())).viewModel;

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getActivity()).finish();
            }
        });

        bottomSheetDialog = new BottomSheetDialog(Objects.requireNonNull(getContext()));
        bottomSheetDialog.setContentView(R.layout.booth_item_booth_sheet);

        adapter = new BoothRecycleViewAdapter(Collections.emptyList(), this);
        boothRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        boothRecyclerView.setAdapter(adapter);

        this.init();

        if (viewModel.boothList.getValue() == null || viewModel.boothList.getValue().getContent() == null) {
            this.loadBooth();
        } else {
            this.adapter.setBoothList(viewModel.boothList.getValue().getContent());
            this.adapter.notifyDataSetChanged();
            shimmerFrameLayout.setVisibility(View.GONE);
            boothRecyclerView.setVisibility(View.VISIBLE);
        }


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        viewModel.boothList.removeObserver(this::handleBoothListChange);
    }

    private void init() {
        viewModel.responseGetAllShop.observe(this, this::handleLoadBooth);
        viewModel.boothList.observe(this, this::handleBoothListChange);
    }


    private void loadBooth() {
        viewModel.getAllShopByUserId();
    }

    private void handleLoadBooth(Event<ApiResponse> event) {
        final ApiResponse apiResponse = event.getContentIfNotHandled();

        if (apiResponse == null) {
            return;
        }

        switch (apiResponse.status) {
            case LOADING:
                shimmerFrameLayout.startShimmer();
                shimmerFrameLayout.setVisibility(View.VISIBLE);
                boothRecyclerView.setVisibility(View.GONE);
                loadListFail.setVisibility(View.GONE);
                break;
            case SUCCESS:
                shimmerFrameLayout.stopShimmer();
                boothRecyclerView.setVisibility(View.VISIBLE);
                shimmerFrameLayout.setVisibility(View.GONE);
                loadListFail.setVisibility(View.GONE);
                break;
            case ERROR:
                shimmerFrameLayout.stopShimmer();
                boothRecyclerView.setVisibility(View.GONE);
                shimmerFrameLayout.setVisibility(View.GONE);
                loadListFail.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void handleBoothListChange(Event<List<BoothItemData>> event) {

        List<BoothItemData> boothList = event.getContentIfNotHandled();

        if (boothList == null) return;

        this.adapter.setBoothList(boothList);
        this.adapter.notifyDataSetChanged();

    }

    @Override
    public void settingsOption() {
        bottomSheetDialog.show();
    }

    @Override
    public void navigateToBoothDetail(Integer id) {
        Intent intent = BoothDetailActivity.createIntent(getContext(), id);
        startActivity(intent);
    }

    @OnClick(R.id.create_booth_linear)
    public void createBooth() {
        ((BoothActivity) Objects.requireNonNull(getActivity())).replaceCreateBoothFragment();
    }

    @OnClick(R.id.load_list_fail)
    public void tryLoad() {
        viewModel.getAllShopByUserId();
    }

}