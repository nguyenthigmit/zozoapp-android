package com.zozo.screens.booth;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.zozo.R;
import com.zozo.databinding.CreateBoothFmBinding;
import com.zozo.event.Event;
import com.zozo.utils.ApiResponse;

import java.util.Objects;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.zozo.utils.ActivityUtils.hideSoftKeyboard;

public class CreateBoothFragment extends Fragment {

    @BindView(R.id.toolbar)
    MaterialToolbar toolbar;

    @BindView(R.id.create_shop_btn)
    CircularProgressButton createShopBtn;

    private BoothViewModel viewModel;

    public static CreateBoothFragment newInstance() {
        CreateBoothFragment createBoothFragment = new CreateBoothFragment();
        Bundle args = new Bundle();
        createBoothFragment.setArguments(args);
        return createBoothFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.create_booth_fm, container, false);
        ButterKnife.bind(this, view);
        CreateBoothFmBinding binding = CreateBoothFmBinding.bind(view);
        viewModel = ((BoothActivity) getActivity()).viewModel;
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
            }
        });

        viewModel.responseCreateShop.observe(this, this::handleCreateBooth);

        return view;
    }

    public void handleCreateBooth(Event<ApiResponse> event) {
        ApiResponse apiResponse = event.getContentIfNotHandled();

        if(apiResponse == null) return;

        switch (apiResponse.status) {
            case LOADING:
                createShopBtn.startAnimation(() -> null);

                break;
            case SUCCESS:
                createShopBtn.revertAnimation(() -> null);
                createShopBtn.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.rounded_edittext));
                hideSoftKeyboard(Objects.requireNonNull(getActivity()));
                MaterialAlertDialogBuilder successBuilder = new MaterialAlertDialogBuilder(getContext());
                successBuilder.setTitle(getResources().getString(R.string.create_booth_success));
                successBuilder.setNegativeButton(getResources().getText(R.string.btn_oke), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack();
                    }
                });
                successBuilder.show();

                break;
            case ERROR:

                createShopBtn.revertAnimation(() -> {
                    createShopBtn.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_edittext));
                    return null;
                });

                MaterialAlertDialogBuilder errorBuilder = new MaterialAlertDialogBuilder(getContext());
                errorBuilder.setTitle(getResources().getString(R.string.create_booth));
                errorBuilder.setMessage(Objects.requireNonNull(apiResponse.error).getMessage());
                errorBuilder.setNegativeButton(getResources().getText(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                errorBuilder.show();

                break;
        }

    }

    @OnClick(R.id.create_shop_btn)
    public void createShop() {
        viewModel.createShop();
    }

}
