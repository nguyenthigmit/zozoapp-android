package com.zozo.screens.booth;

public interface TaskBooth {

    void settingsOption();

    void navigateToBoothDetail(Integer id);

}
