package com.zozo.screens.booth.detail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.fenchtose.nocropper.ImageCache;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;
import com.zozo.R;
import com.zozo.ViewModelFactory;
import com.zozo.ZozoApplication;
import com.zozo.screens.booth.BoothViewModel;
import com.zozo.screens.booth.adapter.BoothDetailAdapter;
import com.zozo.screens.booth.fragments.GalleryFragment;
import com.zozo.screens.booth.fragments.MessageFragment;
import com.zozo.screens.booth.fragments.OrderFragment;
import com.zozo.screens.booth.fragments.ProductFragment;
import com.zozo.screens.booth.fragments.ShopFragment;
import com.zozo.constant.KeyConstant;
import com.zozo.imagepicker.CropImageActivity;
import com.zozo.model.BoothItemData;
import com.zozo.utils.IntentHelpers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.zozo.constant.KeyConstant.CROP_IMAGE_AVATAR_REQUEST_CODE;
import static com.zozo.constant.KeyConstant.CROP_IMAGE_COVER_REQUEST_CODE;
import static com.zozo.constant.KeyConstant.ID;
import static com.zozo.constant.KeyConstant.PICK_IMAGE_AVATAR_REQUEST_CODE;
import static com.zozo.constant.KeyConstant.PICK_IMAGE_COVER_REQUEST_CODE;
import static com.zozo.constant.KeyConstant.TYPE;
import static com.zozo.constant.KeyConstant.URI;
import static com.zozo.constant.KeyConstant.URL;

public class BoothDetailActivity extends AppCompatActivity {

    private int boothId;

    @BindView(R.id.toolbar)
    MaterialToolbar toolbar;

    @BindView(R.id.avatar_image)
    CircleImageView avatarImage;

    @BindView(R.id.cover_image)
    ImageView coverImage;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.booth_detail_viewpager)
    ViewPager viewPager;

    @BindView(R.id.booth_detail_tablayout)
    TabLayout tabLayout;

    @Inject
    ViewModelFactory factory;

    BoothViewModel viewModel;
    BoothItemData data;
    BoothDetailAdapter adapter;


    public static Intent createIntent(Context context, Integer id) {
        Intent intent = new Intent(context, BoothDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(ID, id);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booth_detail_act);
        ButterKnife.bind(this);
        ((ZozoApplication) getApplication()).getAppComponent().doInjection(this);
        viewModel = ViewModelProviders.of(this, factory).get(BoothViewModel.class);
        this.handleIntent();
        data = viewModel.getBoothItemData(this.boothId);
        this.initToolbar();
        this.initFragmentAdapter();
        this.handleCardAndAvatarImage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.booth_detail_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_AVATAR_REQUEST_CODE) {
                assert data != null;
                startImageCrop(CROP_IMAGE_AVATAR_REQUEST_CODE, data.getData(), this.data.getAvatar());

            } else if (requestCode == PICK_IMAGE_COVER_REQUEST_CODE) {
                assert data != null;
                startImageCrop(CROP_IMAGE_COVER_REQUEST_CODE, data.getData(), this.data.getAvatar());

            } else if (requestCode == CROP_IMAGE_AVATAR_REQUEST_CODE) {
                assert data != null;
                avatarImage.setImageURI(data.getData());
            } else if (requestCode == CROP_IMAGE_COVER_REQUEST_CODE) {
                assert data != null;
                java.net.URL url = (java.net.URL) data.getSerializableExtra(URL);
                int byteCount = data.getIntExtra(KeyConstant.COUNT, 0);
                Bitmap bitmap = new ImageCache(getCacheDir(), byteCount).readFromDiskCache(url);
                coverImage.setImageBitmap(bitmap);
            }
        }
    }

    public void handleIntent() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle == null) {
            finish();
        }

        this.boothId = bundle != null ? bundle.getInt(ID) : 0;


    }

    @OnClick(R.id.avatar_image)
    public void onTapAvatar() {
        this.startImagePicker(PICK_IMAGE_AVATAR_REQUEST_CODE);
    }

    @OnClick(R.id.cover_image)
    public void onTapCover() {
        this.startImagePicker(PICK_IMAGE_COVER_REQUEST_CODE);
    }

    void startImagePicker(int requestCode) {
        Intent intent = IntentHelpers.getPickImageChooserIntent(this, getResources().getString(R.string.pick_image), true, true);
        startActivityForResult(intent, requestCode);
    }

    void startImageCrop(int requestCode, Uri data, String nowAvatarUrl) {
        Intent intent = new Intent(this, CropImageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(URI, data);
        bundle.putInt(ID, this.boothId);
        bundle.putString(URL, nowAvatarUrl);

        if (requestCode == CROP_IMAGE_AVATAR_REQUEST_CODE) {
            bundle.putString(TYPE, CropImageActivity.AVATAR_TYPE);
        } else {
            bundle.putString(TYPE, CropImageActivity.COVER_TYPE);
        }

        intent.putExtras(bundle);

        startActivityForResult(intent, requestCode);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        collapsingToolbarLayout.setTitleEnabled(true);
        collapsingToolbarLayout.setTitle(data.getName());
        collapsingToolbarLayout.setExpandedTitleTextAppearance(android.R.style.TextAppearance_DeviceDefault_Medium);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.white));
        collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
        collapsingToolbarLayout.setExpandedTitleGravity(Gravity.CENTER);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    private void handleCardAndAvatarImage() {

        if (data.getAvatar() != null && !data.getAvatar().equals("")) {
            Picasso.get().load(data.getAvatar()).placeholder(new ColorDrawable(getResources().getColor(R.color.colorGrey))).error(R.drawable.default_avatar_shop).into(avatarImage);
        } else {
            Picasso.get().load(R.drawable.default_avatar_shop).into(avatarImage);
        }

        if (data.getCardImage() != null && !data.getCardImage().equals("")) {
            Picasso.get().load(data.getCardImage() != null ? data.getCardImage() : "").placeholder(new ColorDrawable(getResources().getColor(R.color.colorGrey))).error(R.drawable.default_cover_shop).into(coverImage);
        } else {
            Picasso.get().load(R.drawable.default_cover_shop).into(coverImage);
        }

    }

    private void initFragmentAdapter() {

        List<Fragment> fragments = new ArrayList<>();
        fragments.add(ProductFragment.newInstance());
        fragments.add(ShopFragment.newInstance());
        fragments.add(GalleryFragment.newInstance());
        fragments.add(MessageFragment.newInstance());
        fragments.add(OrderFragment.newInstance());

        adapter = new BoothDetailAdapter(getSupportFragmentManager(), this, fragments);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

}
