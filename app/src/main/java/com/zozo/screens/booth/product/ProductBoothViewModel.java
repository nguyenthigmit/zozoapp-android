package com.zozo.screens.booth.product;

import android.text.Editable;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.zozo.source.Repository;

public class ProductBoothViewModel extends ViewModel {

    private Repository repository;

    public final MutableLiveData<String> nameProduct = new MutableLiveData<>();
    public final MutableLiveData<String> costProduct = new MutableLiveData<>();
    public final MutableLiveData<String> promotionProduct = new MutableLiveData<>();
    public final MutableLiveData<String> unitProduct = new MutableLiveData<>();
    public final MutableLiveData<String> shortDescriptionProduct = new MutableLiveData<>();
    public final MutableLiveData<String> detailDescriptionProduct = new MutableLiveData<>();
    public final MutableLiveData<String> guaranteeProduct = new MutableLiveData<>();
    public final MutableLiveData<String> sizeProduct = new MutableLiveData<>();
    public final MutableLiveData<String> colorProduct = new MutableLiveData<>();

    public final MutableLiveData<Boolean> disabledCreated = new MutableLiveData<>();
    public final MutableLiveData<Boolean> disabledColorAdd = new MutableLiveData<>();
    public final MutableLiveData<Boolean> disabledSizeAdd = new MutableLiveData<>();

    public ProductBoothViewModel(Repository repository) {
        this.repository = repository;
        disabledColorAdd.setValue(true);
        disabledSizeAdd.setValue(true);
    }

    public void afterChangedText(Editable s) {


    }

    public void afterChangedTextColorProduct(Editable s) {

        if (disabledColorAdd.getValue() == null) {
            return;
        }

        if (s == null || s.toString().trim().equals("")) {
            if (!this.disabledColorAdd.getValue()) {
                this.disabledColorAdd.setValue(true);
                return;
            }
        }
        if (this.disabledColorAdd.getValue()) {
            this.disabledColorAdd.setValue(false);
        }

    }

    public void afterChangedTextSizeProduct(Editable s) {

        if (disabledSizeAdd.getValue() == null) {
            return;
        }

        if (s == null || s.toString().trim().equals("")) {
            if (!this.disabledSizeAdd.getValue()) {
                this.disabledSizeAdd.setValue(true);
                return;
            }
        }
        if (this.disabledSizeAdd.getValue()) {
            this.disabledSizeAdd.setValue(false);
        }

    }



}
