package com.zozo.screens.booth.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

public class GalleryFragment extends Fragment {

    public static GalleryFragment newInstance() {
        GalleryFragment galleryFragment = new GalleryFragment();
        Bundle bundle = new Bundle();
        galleryFragment.setArguments(bundle);

        return galleryFragment;
    }
}
