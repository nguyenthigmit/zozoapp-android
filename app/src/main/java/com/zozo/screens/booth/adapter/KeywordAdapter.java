package com.zozo.screens.booth.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zozo.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class KeywordAdapter extends RecyclerView.Adapter<ViewHolder> implements Callback {

    private List<String> keywords;

    public KeywordAdapter(List<String> keywords) {
        super();
        this.keywords = keywords;
    }

       @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.keyword_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.callback = this;
        holder.name = this.keywords.get(position);
        holder.index = position;

        holder.textView.setText(holder.name);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return this.keywords != null ? this.keywords.size() : 0;
    }


    public void add(String string) {
        this.keywords.add(string);
        notifyDataSetChanged();
    }

    @Override
    public void onRemove(String name, int index) {
        this.keywords.remove(index);
        notifyDataSetChanged();
    }

    public boolean checkDuplicate(String name) {
        return this.keywords.contains(name);
    }
}

class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.name_tv)
    TextView textView;

    Callback callback;
    String name;
    int index;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @OnClick(R.id.keyword_container)
    public void onClickContainer() {
       callback.onRemove(name, index);
    }
}

interface Callback {
    void onRemove(String name, int index);
}

