package com.zozo.screens.booth;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.zozo.R;
import com.zozo.ViewModelFactory;
import com.zozo.ZozoApplication;

import javax.inject.Inject;

import butterknife.ButterKnife;

import static com.zozo.utils.ActivityUtils.setupHidenKeyboarTouchOutside;

public class BoothActivity extends AppCompatActivity {

    ListBoothFragment listBoothFragment;
    CreateBoothFragment createBoothFragment;

    @Inject
    ViewModelFactory factory;

    BoothViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.booth_act);
        ButterKnife.bind(this);
        setupHidenKeyboarTouchOutside(this, findViewById(R.id.booth_container));

        ((ZozoApplication) getApplication()).getAppComponent().doInjection(this);
        viewModel = ViewModelProviders.of(this, factory).get(BoothViewModel.class);

        initFragment();
        replaceListBoothFragment();

    }

    void initFragment() {
        listBoothFragment = ListBoothFragment.newInstance();
        createBoothFragment = CreateBoothFragment.newInstance();

    }

    void replaceListBoothFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.booth_container, listBoothFragment).addToBackStack("list").commit();
    }

    void replaceCreateBoothFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.booth_container, CreateBoothFragment.newInstance()).addToBackStack("create").commit();
    }

}
