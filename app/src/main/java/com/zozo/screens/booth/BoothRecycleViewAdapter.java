package com.zozo.screens.booth;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.zozo.R;
import com.zozo.model.BoothItemData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class BoothRecycleViewAdapter extends RecyclerView.Adapter<BoothViewHolder> {

    private List<BoothItemData> boothItemDataList;
    private TaskBooth taskBooth;

    public BoothRecycleViewAdapter(List<BoothItemData> boothItemDataList, TaskBooth taskBooth) {
        this.boothItemDataList = boothItemDataList;
        this.taskBooth = taskBooth;
    }

    @NonNull
    @Override
    public BoothViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.booth_item, parent, false);
        BoothViewHolder viewHolder = new BoothViewHolder(view, this.taskBooth);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BoothViewHolder holder, int position) {
        BoothItemData data = boothItemDataList.get(position);
        Context context = holder.itemView.getContext();
        holder.data = data;

        String url = data.getAvatar() != null && !data.getAvatar().equals("") ? data.getAvatar() : "";

        Picasso.get().load(Uri.parse(url)).error(R.drawable.default_avatar_shop).into(holder.avatarImage);
        holder.titleBoothText.setText(data.getName());
        holder.countFollowText.setText(String.format(context.getResources().getString(R.string.count_follow), data.getTotalFollows()));
        holder.countNewOrderText.setText(String.format(context.getResources().getString(R.string.count_new_orders), data.getCountOrdered()));
        holder.countNewMessageText.setText(String.format(context.getResources().getString(R.string.count_new_message), data.getCountOrdered()));
    }


    @Override
    public int getItemCount() {
        return boothItemDataList.size();
    }

    public void setBoothList(List<BoothItemData> boothItemDataList) {
        this.boothItemDataList = boothItemDataList;
    }

    public List<BoothItemData> getBoothItemDataList() {
        return this.boothItemDataList;
    }

    public BoothItemData getItem(int position) {
        return boothItemDataList.get(position);
    }


}

class BoothViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.avatar_booth_image)
    CircleImageView avatarImage;

    @BindView(R.id.title_booth_text)
    TextView titleBoothText;

    @BindView(R.id.count_follow_text)
    TextView countFollowText;

    @BindView(R.id.count_new_order_text)
    TextView countNewOrderText;

    @BindView(R.id.count_new_message_text)
    TextView countNewMessageText;

    @BindView(R.id.booth_options_image)
    ImageView boothOptionsImage;

    TaskBooth taskBooth;

    BoothItemData data;

    public BoothViewHolder(@NonNull View itemView, TaskBooth taskBooth) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.taskBooth = taskBooth;
    }

    @OnClick(R.id.booth_options_image)
    public void optionsSetting() {
        taskBooth.settingsOption();
    }

    @OnClick(R.id.booth_item_linear)
    public void onTapItem() {
        if (data == null) {
            return;
        }

        taskBooth.navigateToBoothDetail(data.getId());
    }
}
