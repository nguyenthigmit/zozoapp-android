package com.zozo.screens.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zozo.R;

public class ShoppingFragment extends Fragment {

    public static ShoppingFragment newInstance() {
        ShoppingFragment shoppingFragment = new ShoppingFragment();
        Bundle args = new Bundle();
        shoppingFragment.setArguments(args);

        return shoppingFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.shopping_fm, container, false);

        return view;
    }

}
