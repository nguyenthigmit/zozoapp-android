package com.zozo.screens.home;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.zozo.event.Event;
import com.zozo.source.Repository;

public class HomeViewModel extends ViewModel {

    MutableLiveData<Event<Boolean>> navigateToLogin = new MutableLiveData<>();

    private Repository repository;

    public HomeViewModel(Repository repository) {
        this.repository = repository;
    }

    public void logout() {
        repository.logout();
        this.navigateToLogin.setValue(new Event(true));
    }


}
