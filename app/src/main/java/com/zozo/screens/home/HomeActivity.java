package com.zozo.screens.home;

import android.os.Bundle;
import android.view.Menu;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.tabs.TabLayout;
import com.zozo.R;
import com.zozo.ViewModelFactory;
import com.zozo.ZozoApplication;
import com.zozo.databinding.HomeActBinding;
import com.zozo.screens.home.profile.ProfileFragment;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.homeTabLayout)
    TabLayout tabLayout;

    @BindView(R.id.homeViewPager)
    ViewPager viewPager;

    @BindView(R.id.toolbar)
    MaterialToolbar toolbar;

    @Inject
    public ViewModelFactory factory;

    HomeViewModel viewModel;

    HomeViewPagerAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        HomeActBinding binding = DataBindingUtil.setContentView(this, R.layout.home_act);
        ButterKnife.bind(this);

        ((ZozoApplication) getApplication()).getAppComponent().doInjection(this);
        viewModel = ViewModelProviders.of(this, factory).get(HomeViewModel.class);
        setSupportActionBar(toolbar);
        this.initViewPage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    void initViewPage() {
        adapter = new HomeViewPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < adapter.getCount(); i++) {

            Fragment fragment = adapter.getItem(i);

            if (fragment instanceof HomeFragment) {
//                Objects.requireNonNull(tabLayout.getTabAt(i)).setIcon(R.drawable.ic_home_tb);
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(R.layout.home_tb);

            } else if (fragment instanceof ShoppingFragment) {
                Objects.requireNonNull(tabLayout.getTabAt(i)).setIcon(R.drawable.ic_shopping_tb);

            }
//            else if (fragment instanceof MakeMoneyFragment) {
//                Objects.requireNonNull(tabLayout.getTabAt(i)).setIcon(R.drawable.ic_make_money_tb);
//
//            }
            else if (fragment instanceof ProfileFragment) {
                Objects.requireNonNull(tabLayout.getTabAt(i)).setIcon(R.drawable.ic_profile_tb);

            } else if (fragment instanceof NotificationFragment) {
                Objects.requireNonNull(tabLayout.getTabAt(i)).setIcon(R.drawable.ic_notification_tb);

            } else if (fragment instanceof SideMenuFragment) {
                Objects.requireNonNull(tabLayout.getTabAt(i)).setIcon(R.drawable.ic_sidemenu_tb);

            }
        }


    }

}
