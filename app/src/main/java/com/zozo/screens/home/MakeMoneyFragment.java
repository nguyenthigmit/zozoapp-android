package com.zozo.screens.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zozo.R;

public class MakeMoneyFragment extends Fragment {


    public static MakeMoneyFragment newInstance() {
        MakeMoneyFragment makeMoneyFragment = new MakeMoneyFragment();
        Bundle args = new Bundle();
        makeMoneyFragment.setArguments(args);

        return makeMoneyFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.make_money_fm, container, false);

        return view;
    }

}
