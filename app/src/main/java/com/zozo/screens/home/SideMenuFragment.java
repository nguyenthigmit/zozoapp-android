package com.zozo.screens.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zozo.R;
import com.zozo.screens.booth.BoothActivity;
import com.zozo.event.Event;
import com.zozo.screens.login.LoginActivity;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SideMenuFragment extends Fragment {

    HomeViewModel viewModel;

    public static SideMenuFragment newInstance() {
        SideMenuFragment sideMenuFragment = new SideMenuFragment();
        Bundle args = new Bundle();
        sideMenuFragment.setArguments(args);

        return sideMenuFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.side_menu_fm, container, false);

        ButterKnife.bind(this, view);
        viewModel = ((HomeActivity) Objects.requireNonNull(getActivity())).viewModel;

        viewModel.navigateToLogin.observe(this, this::navigateLogin);

        return view;
    }

    @OnClick(R.id.your_booth_linear)
    public void navigateToYourBooth() {
        Intent intent = new Intent(getContext(), BoothActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.logout_text)
    public void logout() {
        viewModel.logout();
    }

    public void navigateLogin(Event<Boolean> eventIsToLogin) {
        Boolean isToLogin = eventIsToLogin.getContentIfNotHandled();

        if(isToLogin == null || !isToLogin) {
            return;
        }

        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();

    }


}
