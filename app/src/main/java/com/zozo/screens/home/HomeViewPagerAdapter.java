package com.zozo.screens.home;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.zozo.screens.home.profile.ProfileFragment;

import java.util.ArrayList;
import java.util.List;

public class HomeViewPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> fragments;

    public HomeViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);

        fragments = new ArrayList<>();
        fragments.add(HomeFragment.newInstance());
        fragments.add(ShoppingFragment.newInstance());
//        fragments.add(MakeMoneyFragment.newInstance());
        fragments.add(ProfileFragment.newInstance());
        fragments.add(NotificationFragment.newInstance());
        fragments.add(SideMenuFragment.newInstance());

    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
