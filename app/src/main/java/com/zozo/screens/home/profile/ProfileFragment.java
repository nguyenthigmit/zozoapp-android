package com.zozo.screens.home.profile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.fenchtose.nocropper.ImageCache;
import com.zozo.R;
import com.zozo.constant.KeyConstant;
import com.zozo.screens.home.HomeActivity;
import com.zozo.imagepicker.CropImageActivity;
import com.zozo.utils.IntentHelpers;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.zozo.constant.KeyConstant.CROP_IMAGE_AVATAR_REQUEST_CODE;
import static com.zozo.constant.KeyConstant.CROP_IMAGE_COVER_REQUEST_CODE;
import static com.zozo.constant.KeyConstant.PICK_IMAGE_AVATAR_REQUEST_CODE;
import static com.zozo.constant.KeyConstant.PICK_IMAGE_COVER_REQUEST_CODE;
import static com.zozo.constant.KeyConstant.TYPE;
import static com.zozo.constant.KeyConstant.URI;
import static com.zozo.constant.KeyConstant.URL;

public class ProfileFragment extends Fragment {

    @BindView(R.id.avatar_image)
    CircleImageView avatarImage;

    @BindView(R.id.cover_image)
    ImageView coverImage;


    ProfileViewModel viewModel;

    public static ProfileFragment newInstance() {
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle args = new Bundle();
        profileFragment.setArguments(args);

        return profileFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fm, container, false);
        ButterKnife.bind(this, view);
        viewModel = ViewModelProviders.of(this, ((HomeActivity) getActivity()).factory).get(ProfileViewModel.class);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == PICK_IMAGE_AVATAR_REQUEST_CODE) {
                assert data != null;
                startImageCrop(CROP_IMAGE_AVATAR_REQUEST_CODE, data.getData());

            } else if (requestCode == PICK_IMAGE_COVER_REQUEST_CODE) {
                assert data != null;
                startImageCrop(CROP_IMAGE_COVER_REQUEST_CODE, data.getData());

            } else if (requestCode == CROP_IMAGE_AVATAR_REQUEST_CODE) {
                assert data != null;
                avatarImage.setImageURI(data.getData());
            } else if (requestCode == CROP_IMAGE_COVER_REQUEST_CODE) {
                assert data != null;
                java.net.URL url = (java.net.URL) data.getSerializableExtra(URL);
                int byteCount = data.getIntExtra(KeyConstant.COUNT, 0);
                Bitmap bitmap = new ImageCache(getContext().getCacheDir(), byteCount).readFromDiskCache(url);
                coverImage.setImageBitmap(bitmap);
            }
        }
    }

    @OnClick(R.id.avatar_image)
    public void onTapAvatar() {
        this.startImagePicker(PICK_IMAGE_AVATAR_REQUEST_CODE);
    }

    @OnClick(R.id.cover_image)
    public void onTapCover() {
        this.startImagePicker(PICK_IMAGE_COVER_REQUEST_CODE);
    }

    void startImagePicker(int requestCode) {
        Intent intent = IntentHelpers.getPickImageChooserIntent(getContext(), getResources().getString(R.string.pick_image), true, true);
        startActivityForResult(intent, requestCode);
    }

    void startImageCrop(int requestCode, Uri data) {
        Intent intent = new Intent(getContext(), CropImageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(URI, data);

        if (requestCode == CROP_IMAGE_AVATAR_REQUEST_CODE) {
            bundle.putString(TYPE, CropImageActivity.AVATAR_TYPE);
        } else {
            bundle.putString(TYPE, CropImageActivity.COVER_TYPE);
        }

        intent.putExtras(bundle);

        startActivityForResult(intent, requestCode);
    }


}
