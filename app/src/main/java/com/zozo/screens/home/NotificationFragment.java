package com.zozo.screens.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.zozo.R;

public class NotificationFragment extends Fragment {


    public static NotificationFragment newInstance() {
        NotificationFragment notificationFragment = new NotificationFragment();
        Bundle args = new Bundle();
        notificationFragment.setArguments(args);

        return notificationFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.notification_fm, container, false);

        return view;
    }

}
