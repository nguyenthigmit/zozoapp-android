package com.zozo.screens.forgotpassword;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.appbar.MaterialToolbar;
import com.zozo.R;
import com.zozo.ViewModelFactory;
import com.zozo.ZozoApplication;
import com.zozo.constant.KeyConstant;
import com.zozo.databinding.ForgotpasswordActBinding;
import com.zozo.screens.login.LoginActivity;
import com.zozo.view.NonSwipeableViewPager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForgotPasswordActivity extends AppCompatActivity implements TaskForgotPassword {


    public static final int EMAIL_FRAGMENT_INDEX = 0;
    public static final int CODE_FRAGMENT_INDEX = 1;
    public static final int CHANGE_PASSWORD_FRAGMENT_INDEX = 2;


    @Inject
    ViewModelFactory factory;

    ForgotpasswordActBinding binding;

    @BindView(R.id.viewPager)
    NonSwipeableViewPager viewPager;

    @BindView(R.id.toolbar)
    MaterialToolbar toolbar;

    ForgotPasswordViewModel viewModel;
    private ForgotPasswordAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = DataBindingUtil.setContentView(this, R.layout.forgotpassword_act);

        ButterKnife.bind(this);

        ((ZozoApplication) getApplication()).getAppComponent().doInjection(this);
        viewModel = ViewModelProviders.of(this, factory).get(ForgotPasswordViewModel.class);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
        this.initToolbar();

        adapter = new ForgotPasswordAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        this.handleIntent();
    }

    void initToolbar() {
        toolbar.setNavigationOnClickListener(v -> {
            Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        });
        toolbar.setTitle(!this.isConfirmAccount() ? R.string.forgot_password : R.string.verify);
    }

    @Override
    public void nextPage() {

        int position = viewPager.getCurrentItem() + 1;
        viewPager.setCurrentItem(position, true);
    }

    @Override
    public void goToPage(int position) {
        viewPager.setCurrentItem(position, false);
    }

    private void handleIntent() {
        Intent intent = getIntent();
        String action = intent.getAction();

        if (action != null && action.equals(KeyConstant.ACTION_VERIFY_ACCOUNT)) {
            viewPager.setCurrentItem(CODE_FRAGMENT_INDEX);
        } else {
            viewPager.setCurrentItem(EMAIL_FRAGMENT_INDEX);

        }
    }

    public boolean isConfirmAccount() {
        Intent intent = getIntent();
        String action = intent.getAction();

        return action != null && action.equals(KeyConstant.ACTION_VERIFY_ACCOUNT);
    }

}
