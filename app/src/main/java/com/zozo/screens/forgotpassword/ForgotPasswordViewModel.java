package com.zozo.screens.forgotpassword;

import android.content.Context;
import android.text.Editable;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.raywenderlich.android.validatetor.ValidateTor;
import com.zozo.R;
import com.zozo.event.Event;
import com.zozo.source.Repository;
import com.zozo.utils.ApiResponse;


import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ForgotPasswordViewModel extends ViewModel {

    public final MutableLiveData<String> emailOrPhone = new MutableLiveData<>();
    public final MutableLiveData<String> code = new MutableLiveData<>();
    public final MutableLiveData<String> password = new MutableLiveData<>();
    public final MutableLiveData<String> confirmPassword = new MutableLiveData<>();

    public final MutableLiveData<Boolean> disabledPasswordBtn = new MutableLiveData<>();
    public final MutableLiveData<Boolean> disabledCodeBtn = new MutableLiveData<>();
    public final MutableLiveData<Boolean> disabledEmailBtn = new MutableLiveData<>();


    public final MutableLiveData<Event<ApiResponse>> forgotPasswordResponse = new MutableLiveData<>();
    public final MutableLiveData<Event<ApiResponse>> validateCodeResponse = new MutableLiveData<>();
    public final MutableLiveData<Event<ApiResponse>> changePasswordResponse = new MutableLiveData<>();

    private Repository repository;
    private String userId = null;
    private final CompositeDisposable disposables = new CompositeDisposable();
    ValidateTor validator = new ValidateTor();

    public ForgotPasswordViewModel(Repository repository) {
        this.repository = repository;
        disabledEmailBtn.setValue(true);
        disabledCodeBtn.setValue(true);
        disabledPasswordBtn.setValue(true);
    }


    public void forgotPassword(boolean isEmailInput) {

        String emailOrPhoneString = emailOrPhone.getValue();
        String email = null;
        String phone = null;

        if (validator.isViPhoneNumber(emailOrPhoneString)) {
            phone = emailOrPhoneString;
        } else if (validator.isEmail(emailOrPhoneString)) {
            email = emailOrPhoneString;
        }

        disposables.add(repository.getTaskRemoteDataSource().forgotPassword(email, phone)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) -> {
                    if (isEmailInput) {
                        forgotPasswordResponse.setValue(new Event<>(ApiResponse.loading()));
                    }
                })
                .subscribe(
                        result -> {
                            if (result.isSuccess()) {
                                this.userId = result.getData().getUserId();
                                if (isEmailInput) {
                                    forgotPasswordResponse.setValue(new Event<>(ApiResponse.success(result)));
                                }
                            } else {
                                if (isEmailInput) {
                                    forgotPasswordResponse.setValue(new Event<>(ApiResponse.error(new Throwable(result.getMessage()))));
                                }
                            }

                        },
                        throwable -> {
                            if (isEmailInput) {
                                forgotPasswordResponse.setValue(new Event<>(ApiResponse.error(throwable)));
                            }
                        }
                ));
    }

    public void createNewPassword(Context context) {

        if (!validator.isAtleastLength(password.getValue().trim(), 6)) {
            changePasswordResponse.setValue(new Event<>(ApiResponse.error(new Throwable(context.getString(R.string.password_at_least_6_character)))));
            return;
        }
        if (!confirmPassword.getValue().trim().equals(password.getValue().trim())) {
            changePasswordResponse.setValue(new Event<>(ApiResponse.error(new Throwable(context.getString(R.string.error_confirm_password_dont_match)))));
            return;
        }

        disposables.add(repository.getTaskRemoteDataSource().createNewPassword(password.getValue(), userId, code.getValue()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) ->
                        changePasswordResponse.setValue(new Event<>(ApiResponse.loading())))
                .subscribe(
                        result -> {
                            if (result.isSuccess()) {
                                this.resetCache();
                                changePasswordResponse.setValue(new Event<>(ApiResponse.success(result)));
                            } else {
                                changePasswordResponse.setValue(new Event<>(ApiResponse.error(new Throwable(result.getMessage()))));
                            }

                        },
                        throwable -> {
                            changePasswordResponse.setValue(new Event<>(ApiResponse.error(throwable)));
                        }
                ));
    }

    public void checkValidCode() {

        if (userId == null) {
            return;
        }

        disposables.add(repository.getTaskRemoteDataSource().checkValidCode(userId, code.getValue()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) ->
                        validateCodeResponse.setValue(new Event<>(ApiResponse.loading())))
                .subscribe(
                        result -> {
                            if (result.isSuccess()) {
                                validateCodeResponse.setValue(new Event<>(ApiResponse.success(result)));
                            } else {
                                validateCodeResponse.setValue(new Event<>(ApiResponse.error(new Throwable(result.getMessage()))));
                            }

                        },
                        throwable -> validateCodeResponse.setValue(new Event<>(ApiResponse.error(throwable)))
                ));
    }

    public void afterTextChangedEmail(Editable s) {
        if (emailOrPhone.getValue() == null || emailOrPhone.getValue().trim().equals("")) {
            disabledEmailBtn.setValue(true);
            return;
        }
        disabledEmailBtn.setValue(false);
    }

    public void afterTextChangedCode(Editable s) {
        if (code.getValue() == null || code.getValue().trim().equals("")) {
            disabledCodeBtn.setValue(true);
            return;
        }
        disabledCodeBtn.setValue(false);
    }

    public void afterTextChangedPassword(Editable s) {
        if (password.getValue() == null || password.getValue().trim().equals("")) {
            disabledPasswordBtn.setValue(true);
            return;
        }

        if (confirmPassword.getValue() == null || confirmPassword.getValue().trim().equals("")) {
            disabledPasswordBtn.setValue(true);
            return;
        }

        disabledPasswordBtn.setValue(false);
    }

    public void confirmAccount(boolean fromLoginActivity) {
        disposables.add(repository.getTaskRemoteDataSource().confirmAccount(fromLoginActivity ? this.userId : repository.authToken.userId, code.getValue()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) ->
                        validateCodeResponse.setValue(new Event<>(ApiResponse.loading())))
                .subscribe(
                        result -> {
                            if (result.isSuccess()) {
                                validateCodeResponse.setValue(new Event<>(ApiResponse.success(result)));
                            } else {
                                validateCodeResponse.setValue(new Event<>(ApiResponse.error(new Throwable(result.getMessage()))));
                            }

                        },
                        throwable -> validateCodeResponse.setValue(new Event<>(ApiResponse.error(throwable)))
                ));
    }

    public void resetCache() {
        emailOrPhone.setValue("");
        code.setValue("");
        password.setValue("");
        confirmPassword.setValue("");
    }

}
