package com.zozo.screens.forgotpassword;

public interface TaskForgotPassword {

    void nextPage();

    void goToPage(int position);

}
