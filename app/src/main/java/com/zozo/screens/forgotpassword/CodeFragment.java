package com.zozo.screens.forgotpassword;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.zozo.R;
import com.zozo.constant.KeyConstant;
import com.zozo.databinding.ForgotpasswordCodeFmBinding;
import com.zozo.event.Event;
import com.zozo.screens.login.LoginActivity;
import com.zozo.utils.ApiResponse;

import java.util.Objects;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.zozo.utils.ActivityUtils.setupHidenKeyboarTouchOutside;


public class CodeFragment extends Fragment {


    ForgotPasswordViewModel viewModel;

    @BindView(R.id.continue_btn)
    CircularProgressButton continueBtn;

    private TaskForgotPassword callback;


    public static CodeFragment newInstance() {
        CodeFragment codeFragment = new CodeFragment();

        Bundle bundle = new Bundle();
        codeFragment.setArguments(bundle);

        return codeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.forgotpassword_code_fm, container, false);
        ButterKnife.bind(this, view);
        setupHidenKeyboarTouchOutside(getActivity(), view.findViewById(R.id.outside_linear));

        ForgotpasswordCodeFmBinding binding = ForgotpasswordCodeFmBinding.bind(view);
        this.viewModel = ((ForgotPasswordActivity) Objects.requireNonNull(getActivity())).viewModel;
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        viewModel.validateCodeResponse.observe(this, this::handleCheckCode);
        callback = (ForgotPasswordActivity) getActivity();
        this.firstSendOTP();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        this.continueBtn.dispose();
    }

    private void handleCheckCode(Event<ApiResponse> event) {
        ApiResponse apiResponse = event.getContentIfNotHandled();

        if (apiResponse == null) {
            return;
        }

        switch (apiResponse.status) {
            case LOADING:
                continueBtn.startAnimation(() -> null);
                break;

            case SUCCESS:
                continueBtn.revertAnimation(() -> null);
                continueBtn.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.rounded_edittext));

                if (((ForgotPasswordActivity) Objects.requireNonNull(getActivity())).isConfirmAccount()) {
                    MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
                    builder.setTitle(getResources().getString(R.string.register));
                    builder.setMessage(Objects.requireNonNull(getResources().getString(R.string.verify_success)));
                    builder.setNegativeButton(getResources().getText(R.string.btn_i_known), (dialog, which) -> {
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    });
                    builder.show();
                } else {
                    callback.nextPage();
                }

                break;

            case ERROR:
                continueBtn.revertAnimation(() -> null);
                continueBtn.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.rounded_edittext));

                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
                builder.setTitle(getResources().getString(((ForgotPasswordActivity) Objects.requireNonNull(getActivity())).isConfirmAccount() ? R.string.register : R.string.error_dialog_verify_title));
                builder.setMessage(Objects.requireNonNull(apiResponse.error).getMessage());
                builder.setNegativeButton(getResources().getText(R.string.btn_cancel), (dialog, which) -> {

                });
                builder.show();
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.continue_btn)
    void onContinuePress() {

        if (((ForgotPasswordActivity) Objects.requireNonNull(getActivity())).isConfirmAccount()) {
            Intent intent = Objects.requireNonNull(getActivity()).getIntent();

            if (intent == null) {
                return;
            }

            String bundle = intent.getStringExtra(KeyConstant.ACTIVITY_NAME_BUNDLE_ID);

            if (bundle == null) {
                bundle = "";
            }

            boolean fromLoginAct = bundle.equals(LoginActivity.TAG);
            this.viewModel.confirmAccount(fromLoginAct);
        } else {
            this.viewModel.checkValidCode();
        }
    }

    @OnClick(R.id.try_again_txt)
    void resendCode() {
        viewModel.forgotPassword(false);
    }


    private void firstSendOTP() {
        Intent intent = Objects.requireNonNull(getActivity()).getIntent();
        String action = intent.getAction();
        String activityBundle = intent.getStringExtra(KeyConstant.ACTIVITY_NAME_BUNDLE_ID);
        String username = intent.getStringExtra(KeyConstant.USERNAME);

        if (action == null || !action.equals(KeyConstant.ACTION_VERIFY_ACCOUNT) || activityBundle == null || !activityBundle.equals(LoginActivity.TAG)) {
            return;
        }
        viewModel.emailOrPhone.setValue(username);
        viewModel.forgotPassword(false);
    }
}
