package com.zozo.screens.forgotpassword;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.zozo.R;
import com.zozo.databinding.ForgotpasswordChangpasswordFrmBinding;
import com.zozo.event.Event;
import com.zozo.screens.login.LoginActivity;
import com.zozo.utils.ApiResponse;

import java.util.Objects;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.zozo.utils.ActivityUtils.setupHidenKeyboarTouchOutside;

public class ChangePasswordFragment extends Fragment {


    ForgotPasswordViewModel viewModel;

    @BindView(R.id.create_new_password_btn)
    CircularProgressButton continueBtn;

    private TaskForgotPassword callback;


    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();

        Bundle bundle = new Bundle();
        changePasswordFragment.setArguments(bundle);

        return changePasswordFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.forgotpassword_changpassword_frm, container, false);
        ButterKnife.bind(this, view);

        setupHidenKeyboarTouchOutside(getActivity(), view.findViewById(R.id.outside_linear));

        ForgotpasswordChangpasswordFrmBinding binding = ForgotpasswordChangpasswordFrmBinding.bind(view);
        this.viewModel = ((ForgotPasswordActivity) Objects.requireNonNull(getActivity())).viewModel;
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        viewModel.changePasswordResponse.observe(this, this::handleChangePassword);
        callback = (ForgotPasswordActivity) getActivity();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        this.continueBtn.dispose();
    }

    public void handleChangePassword(Event<ApiResponse> event) {
        ApiResponse apiResponse = event.getContentIfNotHandled();

        if (apiResponse == null) {
            return;
        }

        switch (apiResponse.status) {
            case LOADING:
                continueBtn.startAnimation(() -> null);
                break;

            case SUCCESS:
                continueBtn.revertAnimation(() -> null);
                continueBtn.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.rounded_edittext));

                MaterialAlertDialogBuilder builderSuccess = new MaterialAlertDialogBuilder(getContext());
                builderSuccess.setTitle(getResources().getString(R.string.dialog_change_password_title));
                builderSuccess.setMessage(getResources().getString(R.string.message_change_password_success));
                builderSuccess.setNegativeButton(getResources().getText(R.string.btn_i_known), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        startActivity(intent);
                        Objects.requireNonNull(getActivity()).finish();
                    }
                });
                builderSuccess.show();
                break;

            case ERROR:
                continueBtn.revertAnimation(() -> {
                    continueBtn.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.rounded_edittext));
                    return null;
                });
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()));
                builder.setTitle(getResources().getString(R.string.dialog_change_password_title));
                builder.setMessage(Objects.requireNonNull(apiResponse.error).getMessage());
                builder.setNegativeButton(getResources().getText(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.create_new_password_btn)
    public void onContinuePress() {
        this.viewModel.createNewPassword(getContext());
    }

    @OnClick(R.id.goto_login_txt)
    public void gotoLogin() {
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        Objects.requireNonNull(getActivity()).finish();
    }

}
