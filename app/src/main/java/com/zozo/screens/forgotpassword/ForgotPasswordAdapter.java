package com.zozo.screens.forgotpassword;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ForgotPasswordAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;

    public ForgotPasswordAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        fragments = new ArrayList<>();
        fragments.add(EmailFragment.newInstance());
        fragments.add(CodeFragment.newInstance());
        fragments.add(ChangePasswordFragment.newInstance());
    }

    @Override
    public int getCount() {
        return fragments.size();
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }


}
