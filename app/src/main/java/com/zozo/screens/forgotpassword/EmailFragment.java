package com.zozo.screens.forgotpassword;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.zozo.R;
import com.zozo.databinding.ForgotpasswordEmailFmBinding;
import com.zozo.event.Event;
import com.zozo.screens.register.RegisterActivity;
import com.zozo.utils.ApiResponse;

import java.util.Objects;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.zozo.utils.ActivityUtils.setupHidenKeyboarTouchOutside;

public class EmailFragment extends Fragment {

    ForgotPasswordViewModel viewModel;

    @BindView(R.id.continue_btn)
    CircularProgressButton continueBtn;

    private TaskForgotPassword callback;


    public static EmailFragment newInstance() {
        EmailFragment emailFragment = new EmailFragment();

        Bundle bundle = new Bundle();
        emailFragment.setArguments(bundle);

        return emailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forgotpassword_email_fm, container, false);
        ButterKnife.bind(this, view);
        ForgotpasswordEmailFmBinding binding = ForgotpasswordEmailFmBinding.bind(view);

        setupHidenKeyboarTouchOutside(getActivity(), view.findViewById(R.id.outside_linear));

        this.viewModel = ((ForgotPasswordActivity) Objects.requireNonNull(getActivity())).viewModel;
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        viewModel.forgotPasswordResponse.observe(this, this::handleSendEmail);
        callback = (ForgotPasswordActivity) getActivity();

        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        this.viewModel.forgotPasswordResponse.removeObserver(this::handleSendEmail);
        this.continueBtn.dispose();
    }

    public void handleSendEmail(Event<ApiResponse> event) {
        ApiResponse apiResponse = event.getContentIfNotHandled();

        if (apiResponse == null) {
            return;
        }

        switch (apiResponse.status) {
            case LOADING:
                continueBtn.startAnimation(() -> null);
                break;

            case SUCCESS:

                if (((ForgotPasswordActivity) Objects.requireNonNull(getActivity())).isConfirmAccount()) {
                    return;
                }

                continueBtn.revertAnimation(() -> null);
                continueBtn.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.rounded_edittext));

                callback.nextPage();

                break;

            case ERROR:
                continueBtn.revertAnimation(() -> null);
                continueBtn.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.rounded_edittext));

                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
                builder.setTitle(getResources().getString(R.string.dialog_change_password_title));
                builder.setMessage(Objects.requireNonNull(apiResponse.error).getMessage());
                builder.setNegativeButton(getResources().getText(R.string.btn_cancel), (dialog, which) -> {

                });
                builder.show();
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.continue_btn)
    public void onContinuePress() {
        this.viewModel.forgotPassword(true);
    }

    @OnClick(R.id.register_text)
    public void gotoRegister() {
        Intent intent = new Intent(getContext(), RegisterActivity.class);
        startActivity(intent);
        Objects.requireNonNull(getActivity()).finish();
    }

}
