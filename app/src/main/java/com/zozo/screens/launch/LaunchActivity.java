package com.zozo.screens.launch;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.zozo.R;
import com.zozo.ViewModelFactory;
import com.zozo.ZozoApplication;
import com.zozo.event.Event;
import com.zozo.screens.home.HomeActivity;
import com.zozo.screens.login.LoginActivity;

import java.util.List;

import javax.inject.Inject;

public class LaunchActivity extends AppCompatActivity {

    @Inject
    ViewModelFactory viewModelFactory;
    private LaunchViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.launch_act);
        ((ZozoApplication) getApplication()).getAppComponent().doInjection(this);

        this.viewModel = ViewModelProviders.of(this, viewModelFactory).get(LaunchViewModel.class);
        this.viewModel.mustNavigateToLogin.observe(this, this::navigate);

        requestStorage();

    }

    void navigate(Event eventNavigateToLogin) {
        Boolean navigateToLogin = (Boolean) eventNavigateToLogin.getContentIfNotHandled();
        Intent intent;
        if (navigateToLogin) {
            intent = new Intent(this, LoginActivity.class);
        } else {
            intent = new Intent(this, HomeActivity.class);
        }
        startActivity(intent);
        finish();
    }

    public void requestStorage() {
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        Dexter.withActivity(this).withPermissions(permissions).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                LaunchActivity.this.viewModel.verifyToken();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).withErrorListener(error -> LaunchActivity.this.viewModel.verifyToken()).check();
    }

}
