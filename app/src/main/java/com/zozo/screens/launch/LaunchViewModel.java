package com.zozo.screens.launch;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.zozo.event.Event;
import com.zozo.source.Repository;
import com.zozo.source.remote.TaskRemoteDataSource;
import com.zozo.utils.ApiResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class LaunchViewModel extends ViewModel {

    public MutableLiveData<Event<Boolean>> mustNavigateToLogin = new MutableLiveData<>();
    public MutableLiveData<Event<ApiResponse>> meResponse = new MutableLiveData<>();


    private Repository repository;

    public LaunchViewModel(Repository repository) {
        this.repository = repository;
    }

    private final CompositeDisposable disposables = new CompositeDisposable();


    public void verifyToken() {

        TaskRemoteDataSource remote = repository.getTaskRemoteDataSource();

        disposables.add(remote.me(repository.authToken.token, repository.authToken.userId, null, null, null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) ->
                        meResponse.setValue(new Event<>(ApiResponse.loading())))
                .subscribe(
                        result -> {
                            // go to home
                            repository.user = result;
                            mustNavigateToLogin.setValue(new Event<>(false));

                        },
                        throwable -> {
                            repository.logout();
                            meResponse.setValue(new Event<>(ApiResponse.error(throwable)));
                            // go to login
                            mustNavigateToLogin.setValue(new Event<>(true));
                        }
                ));
    }


}