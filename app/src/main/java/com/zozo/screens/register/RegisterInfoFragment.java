package com.zozo.screens.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.zozo.R;
import com.zozo.constant.KeyConstant;
import com.zozo.databinding.RegisterInfoFmBinding;
import com.zozo.event.Event;
import com.zozo.screens.forgotpassword.ForgotPasswordActivity;
import com.zozo.screens.login.LoginActivity;
import com.zozo.model.CreateUserResponse;
import com.zozo.model.ResponseModel;
import com.zozo.utils.ApiResponse;

import java.util.Calendar;
import java.util.Objects;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.zozo.utils.ActivityUtils.setupHidenKeyboarTouchOutside;

public class RegisterInfoFragment extends Fragment {


    @BindView(R.id.toolbar)
    MaterialToolbar toolbar;

    @BindView(R.id.create_user_btn)
    CircularProgressButton createUserBtn;

    @BindView(R.id.birthday_text)
    TextView birthdayText;

    RegisterViewModel viewModel;
    RegisterInfoFmBinding binding;
    com.tsongkha.spinnerdatepicker.DatePickerDialog datePickerDialog;
    TaskRegister callback;

    public static RegisterInfoFragment newInstance() {
        RegisterInfoFragment registerInfoFragment = new RegisterInfoFragment();
        Bundle args = new Bundle();
        registerInfoFragment.setArguments(args);

        return registerInfoFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_info_fm, container, false);

        binding = RegisterInfoFmBinding.bind(view);
        ButterKnife.bind(this, view);
        setupHidenKeyboarTouchOutside(getActivity(), view);
        this.viewModel = ((RegisterActivity) Objects.requireNonNull(getActivity())).viewModel;
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(getActivity());
        viewModel.registerResponse.observe(this, this::handleCreateUser);
        callback = (TaskRegister) getActivity();
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new SpinnerDatePickerDialogBuilder()
                .context(getContext())
                .callback((view1, year1, monthOfYear, dayOfMonth) -> {
                    viewModel.setBirthdayText(String.format(getString(R.string.date_format), dayOfMonth, monthOfYear + 1, year1));
                })
                .defaultDate(1990, 0, 1)
                .minDate(1935, 0, 1)
                .maxDate(year, month, day)
                .build();

        // set default
        viewModel.setBirthdayText(String.format(getString(R.string.date_format), 1, 1, 1990));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
                Objects.requireNonNull(getActivity()).finish();
            }
        });


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        createUserBtn.dispose();
    }

    @OnClick(R.id.create_user_btn)
    public void createUserOnPress() {
        viewModel.createUser(getContext());
    }

    @OnClick(R.id.birthday_text)
    public void birthdayOnPress() {
        datePickerDialog.show();
    }

    public void handleCreateUser(Event<ApiResponse> event) {
        ApiResponse apiResponse = event.getContentIfNotHandled();

        if (apiResponse == null) {
            return;
        }

        switch (apiResponse.status) {
            case LOADING:
                this.createUserBtn.startAnimation(() -> null);
                break;
            case SUCCESS:
                createUserBtn.revertAnimation(() -> null);
                createUserBtn.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.rounded_edittext));
                MaterialAlertDialogBuilder successBuilder = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()));
                successBuilder.setTitle(R.string.register);

                if (Objects.requireNonNull(this.viewModel.isRegisterByPhone.getValue()).getContentIfNotHandled()) {
                    successBuilder.setMessage(getString(R.string.register_verify_phone));
                    successBuilder.setPositiveButton(getResources().getText(R.string.btn_i_known), (dialog, which) -> {
                        this.navigateToVerify();
                    });
                } else {
                    successBuilder.setMessage(getString(R.string.register_verify_email));
                    successBuilder.setPositiveButton(getResources().getText(R.string.btn_i_known), (dialog, which) -> {
                        Objects.requireNonNull(getActivity()).finish();
                    });
                }

                successBuilder.show();


                break;
            case ERROR:
                createUserBtn.revertAnimation(() -> null);
                createUserBtn.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.rounded_edittext));

                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()));

                assert apiResponse.data != null;
                ResponseModel<CreateUserResponse> response = (ResponseModel<CreateUserResponse>) apiResponse.data;

                if (apiResponse.error.getMessage().equals("Phone number of user exists in the system! Please check OTP from your phone!")) {
                    builder.setMessage(getString(R.string.register_phone_number_of_exists));
                    builder.setPositiveButton(getResources().getText(R.string.btn_i_known), (dialog, which) -> {
                    });
                    builder.show();
                    return;
                }

                builder.setTitle(getResources().getString(R.string.error_dialog_register_title));
                builder.setMessage(Objects.requireNonNull(apiResponse.error).getMessage());
                builder.setNegativeButton(getResources().getText(R.string.btn_cancel), (dialog, which) -> {

                });
                builder.show();

                break;

        }
    }

    private void navigateToVerify() {
        Intent intent = new Intent(getContext(), ForgotPasswordActivity.class);
        intent.setAction(KeyConstant.ACTION_VERIFY_ACCOUNT);
        startActivity(intent);
        Objects.requireNonNull(getActivity()).finish();
    }

}
