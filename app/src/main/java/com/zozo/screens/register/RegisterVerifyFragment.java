package com.zozo.screens.register;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.zozo.R;
import com.zozo.databinding.RegisterVerifyFmBinding;
import com.zozo.deeplink.DeepLinker;
import com.zozo.event.Event;
import com.zozo.utils.ApiResponse;
import com.zozo.utils.ResponseStatus;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterVerifyFragment extends Fragment {


    RegisterViewModel viewModel;

    public static RegisterVerifyFragment newInstance() {
        RegisterVerifyFragment registerVerifyFragment = new RegisterVerifyFragment();

        Bundle bundle = new Bundle();
        registerVerifyFragment.setArguments(bundle);

        return registerVerifyFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_verify_fm, container, false);

        RegisterVerifyFmBinding binding = RegisterVerifyFmBinding.bind(view);
        ButterKnife.bind(this, view);
        this.viewModel = ((RegisterActivity) Objects.requireNonNull(getActivity())).viewModel;
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
        this.viewModel.confirmResponse.observe(this, this::handleConfirm);

        this.verifyApi();
        return view;
    }

    @OnClick(R.id.goto_login_btn)
    public void goToLogin() {
        Objects.requireNonNull(getActivity()).finish();
    }

    public void handleConfirm(Event<ApiResponse> event) {
        final ApiResponse apiResponse = event.getContentIfNotHandled();

        if (apiResponse == null) {
            return;
        }

        if (apiResponse.status == ResponseStatus.ERROR) {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
            builder.setTitle(getResources().getString(R.string.error_dialog_verify_title));
            builder.setMessage(Objects.requireNonNull(apiResponse.error).getMessage());
            builder.setNegativeButton(getResources().getText(R.string.btn_i_known), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    RegisterVerifyFragment.this.goToLogin();
                }
            });
            builder.show();
        }

    }

    private void verifyApi() {

        Bundle bundle = getArguments();

        if (bundle == null) {
            return;
        }

        ArrayList<String> userIds = (ArrayList<String>) bundle.getSerializable(DeepLinker.KEY_USER_ID);
        ArrayList<String> codes = (ArrayList<String>) bundle.getSerializable(DeepLinker.KEY_CODE);


        if (userIds == null || codes == null || userIds.size() < 1 || codes.size() < 1) {
            return;
        }

        viewModel.emailConfirm(codes.get(0), userIds.get(0));

    }
}
