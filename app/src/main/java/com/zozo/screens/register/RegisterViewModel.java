package com.zozo.screens.register;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;

import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.raywenderlich.android.validatetor.ValidateTor;
import com.zozo.R;
import com.zozo.event.Event;
import com.zozo.model.AuthToken;
import com.zozo.model.CreateUserParams;
import com.zozo.model.UserType;
import com.zozo.source.Repository;
import com.zozo.utils.ApiResponse;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class RegisterViewModel extends ViewModel {

    public final MutableLiveData<String> lastName = new MutableLiveData<>();
    public final MutableLiveData<String> firstName = new MutableLiveData<>();
    public final MutableLiveData<String> username = new MutableLiveData<>();
    public final MutableLiveData<String> password = new MutableLiveData<>();
    public final MutableLiveData<String> confirmPassword = new MutableLiveData<>();
    public final MutableLiveData<String> birthday = new MutableLiveData<>();

    public final MutableLiveData<Boolean> showLoadingVerify = new MutableLiveData<>();
    public final MutableLiveData<Boolean> disabledCreateBtn = new MutableLiveData<>();

    public final ObservableInt messageErrorPassword = new ObservableInt(R.string.empty_password);
    public final ObservableInt messageErrorConfirmPassword = new ObservableInt(R.string.empty_confirm_password);


    public final MutableLiveData<Event<ApiResponse>> registerResponse = new MutableLiveData<>();
    public final MutableLiveData<Event<ApiResponse>> confirmResponse = new MutableLiveData<>();
    public final MutableLiveData<Event<Boolean>> isRegisterByPhone = new MutableLiveData<>();


    private final CompositeDisposable disposables = new CompositeDisposable();
    private final Repository repository;
    private ValidateTor validate = new ValidateTor();

    public RegisterViewModel(Repository repository) {
        this.repository = repository;
        showLoadingVerify.setValue(true);
        disabledCreateBtn.setValue(true);
    }

    public void setBirthdayText(String date) {
        this.birthday.setValue(date);
    }


    private void resetInfoCreate() {
        lastName.setValue("");
        firstName.setValue("");
        username.setValue("");
        password.setValue("");
        confirmPassword.setValue("");
        birthday.setValue("");
    }


    public void createUser(Context context) {

        if (!validate.isAtleastLength(password.getValue().trim(), 6)) {
            registerResponse.setValue(new Event<>(ApiResponse.error(new Throwable(context.getString(R.string.password_at_least_6_character)))));
            return;
        }

        if (!confirmPassword.getValue().equals(password.getValue())) {
            registerResponse.setValue(new Event<>(ApiResponse.error(new Throwable(context.getString(R.string.error_confirm_password_dont_match)))));
            return;
        }

        String usernameString = username.getValue();
        String emailString = username.getValue();

        CreateUserParams params = new CreateUserParams();

        final boolean validatePhone = validate.isViPhoneNumber(usernameString);

        if (validatePhone) {
            emailString = usernameString + "@gmail.com";
            params.setPhoneNumber(usernameString);
        }

        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("YYYY/MM/dd");
        String formatDate = format.format(Date.parse(birthday.getValue()));
        params.setLastName(lastName.getValue());
        params.setFirstName(firstName.getValue());
        params.setUserName(usernameString);
        params.setBirthday(formatDate);
        params.setPassword(password.getValue());
        params.setPasswordConfirm(password.getValue());
        params.setEmail(emailString);

        params.setUserType(UserType.Customer.getValue());

        disposables.add(repository.getTaskRemoteDataSource().createUser(params).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) ->
                        registerResponse.setValue(new Event<>(ApiResponse.loading())))
                .subscribe(result -> {
                    if (!result.isSuccess()) {
                        registerResponse.setValue(new Event<>(ApiResponse.error(new Throwable(result.getMessage()))));
                    } else {
                        this.resetInfoCreate();
                        repository.authToken = new AuthToken(null, result.getData().getUserId());
                        isRegisterByPhone.setValue(new Event<>(validatePhone));
                        registerResponse.setValue(new Event<>(ApiResponse.success(result.getData())));
                    }
                }, throwable -> {
                    registerResponse.setValue(new Event<>(ApiResponse.error(throwable)));
                })
        );
    }

    public void emailConfirm(String code, String userId) {
        disposables.add(repository.getTaskRemoteDataSource().emailConfirm(code, userId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) -> this.showLoadingVerify.setValue(true))
                .subscribe(result -> {
                    this.showLoadingVerify.setValue(false);
                    confirmResponse.setValue(new Event<>(ApiResponse.success(result)));
                }, throwable -> {
                    confirmResponse.setValue(new Event<>(ApiResponse.error(throwable)));
                })
        );
    }

    public void afterTextChanged(Editable s) {
        if (lastName.getValue() == null || lastName.getValue().trim().equals("")) {
            this.disabledCreateBtn.setValue(true);
            return;

        }

        if (firstName.getValue() == null || firstName.getValue().trim().equals("")) {
            this.disabledCreateBtn.setValue(true);
            return;
        }

        if (username.getValue() == null || username.getValue().trim().equals("")) {
            this.disabledCreateBtn.setValue(true);
            return;

        }


        if (password.getValue() == null || password.getValue().trim().equals("")) {
            this.disabledCreateBtn.setValue(true);
            return;

        }

        if (confirmPassword.getValue() == null || confirmPassword.getValue().trim().equals("")) {
            this.disabledCreateBtn.setValue(true);
            return;

        }

        if (birthday.getValue() == null || birthday.getValue().trim().equals("")) {
            this.disabledCreateBtn.setValue(true);
            return;

        }

        if (!validate.isViPhoneNumber(username.getValue()) && !validate.isEmail(username.getValue())) {
            this.disabledCreateBtn.setValue(true);
            return;
        }

        this.disabledCreateBtn.setValue(false);

    }

}
