package com.zozo.screens.register;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.zozo.R;
import com.zozo.ViewModelFactory;
import com.zozo.ZozoApplication;
import com.zozo.databinding.RegisterActBinding;
import com.zozo.deeplink.DeepLinker;
import com.zozo.utils.ActivityUtils;

import javax.inject.Inject;

import butterknife.ButterKnife;


public class RegisterActivity extends AppCompatActivity implements TaskRegister {

    @Inject
    ViewModelFactory factory;

    RegisterViewModel viewModel;

    RegisterActBinding binding;

    RegisterVerifyFragment registerVerifyFragment;
    RegisterInfoFragment registerInfoFragment;
    DeepLinker deepLinker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.binding = DataBindingUtil.setContentView(this, R.layout.register_act);
        ButterKnife.bind(this);

        ((ZozoApplication) getApplication()).getAppComponent().doInjection(this);
        viewModel = ViewModelProviders.of(this, factory).get(RegisterViewModel.class);

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
        deepLinker = new DeepLinker();
        this.initFragment();
        this.handleIntent();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        this.handleIntent();


    }

    void initFragment() {
        this.registerInfoFragment = RegisterInfoFragment.newInstance();
        this.registerVerifyFragment = RegisterVerifyFragment.newInstance();
    }

    @Override
    public void replaceRegisterInfoFragment() {
        ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(), registerInfoFragment, R.id.register_container);
    }

    @Override
    public void replaceRegisterVerifyFragment() {
        ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(), registerVerifyFragment, R.id.register_container);

    }

    public void handleIntent() {
        Intent intent = getIntent();

        Uri uri = intent.getData();

        if (uri == null) {
            this.replaceRegisterInfoFragment();
            return;
        }

        Bundle data = deepLinker.buildBundle(uri);
        if (data == null) {
            return;
        }

        DeepLinker.Link link = DeepLinker.getLinkFromBundle(data);
        if (link == null) {
            return;
        }

        if (link == DeepLinker.Link.EMAIL_CONFIRM) {
            this.registerVerifyFragment.setArguments(data);
            this.replaceRegisterVerifyFragment();
        }

    }

}
