package com.zozo.screens.login;

import android.text.Editable;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.raywenderlich.android.validatetor.ValidateTor;
import com.zozo.event.Event;
import com.zozo.source.Repository;
import com.zozo.source.remote.TaskRemoteDataSource;
import com.zozo.utils.ApiResponse;

import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class LoginViewModel extends ViewModel {

    public final MutableLiveData<Event<ApiResponse>> loginResponse = new MutableLiveData<>();
    public final MutableLiveData<String> username = new MutableLiveData<>();
    public final MutableLiveData<String> password = new MutableLiveData<>();
    public final MutableLiveData<Boolean> disabledButton = new MutableLiveData<>();
    public final MutableLiveData<Event<Boolean>> goToVerifyScreen = new MutableLiveData<>();
    private Repository repository;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private ValidateTor validate = new ValidateTor();

    public LoginViewModel(Repository repository) {
        this.repository = repository;
        disabledButton.setValue(true);
    }

    void login() {
        TaskRemoteDataSource remote = repository.getTaskRemoteDataSource();
        disposables.add(remote.executeLogin(username.getValue().trim(), password.getValue().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) ->
                        loginResponse.setValue(new Event<>(ApiResponse.loading())))
                .subscribe(
                        result -> {
                            if(result.isSuccess()) {
                                Map<String, String> data = result.getData();
                                this.resetCacheEdit();
                                repository.persistAuth(data.get("auth_token"), data.get("id"));
                                loginResponse.setValue(new Event<>(ApiResponse.success(result)));
                            } else {
                                if(result.getMessage().contains("Account is inactive!")) {
                                    goToVerifyScreen.setValue(new Event<>(true));
                                } else {
                                    loginResponse.setValue(new Event<>(ApiResponse.error(new Throwable(result.getMessage()))));
                                }

                            }

                        },
                        throwable -> loginResponse.setValue(new Event<>(ApiResponse.error(throwable)))
                ));
    }


    public void afterTextChanged(Editable s) {
        if (username.getValue() == null || username.getValue().trim().equals("")) {
            disabledButton.setValue(true);
            return;
        }

        if (password.getValue() == null || password.getValue().trim().equals("")) {
            disabledButton.setValue(true);
            return;
        }

        disabledButton.setValue(false);
    }

    public void resetCacheEdit() {
        this.username.setValue("");
        this.password.setValue("");
    }
}
