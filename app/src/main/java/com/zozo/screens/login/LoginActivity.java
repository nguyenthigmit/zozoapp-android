package com.zozo.screens.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.zozo.R;
import com.zozo.ViewModelFactory;
import com.zozo.ZozoApplication;
import com.zozo.constant.KeyConstant;
import com.zozo.databinding.LoginActBinding;
import com.zozo.event.Event;
import com.zozo.screens.forgotpassword.ForgotPasswordActivity;
import com.zozo.screens.home.HomeActivity;
import com.zozo.screens.register.RegisterActivity;
import com.zozo.utils.ApiResponse;

import java.util.Objects;

import javax.inject.Inject;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.zozo.utils.ActivityUtils.setupHidenKeyboarTouchOutside;

public class LoginActivity extends AppCompatActivity {


    public static final String TAG = "LoginActivity";

    @Inject
    ViewModelFactory factory;

    private LoginViewModel viewModel;
    private LoginActBinding loginActBinding;

    @BindView(R.id.forgot_text)
    TextView forgotText;

    @BindView(R.id.register_text)
    TextView registerText;

    @BindView(R.id.login_btn)
    CircularProgressButton loginBtn;

    @BindView(R.id.toolbar)
    MaterialToolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.loginActBinding = DataBindingUtil.setContentView(this, R.layout.login_act);
        ButterKnife.bind(this);
        setupHidenKeyboarTouchOutside(this, findViewById(R.id.outside_linear));

        ((ZozoApplication) getApplication()).getAppComponent().doInjection(this);
        this.viewModel = ViewModelProviders.of(this, factory).get(LoginViewModel.class);
        this.loginActBinding.setViewModel(this.viewModel);
        this.loginActBinding.setLifecycleOwner(this);

        loginBtn.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_edittext));
        viewModel.loginResponse.observe(this, this::handleApiLogin);
        viewModel.goToVerifyScreen.observe(this, this::handleVerifyAccount);
        toolbar.setTitleTextColor(Color.WHITE);

    }

    public void handleApiLogin(Event event) {

        ApiResponse apiResponse = (ApiResponse) event.getContentIfNotHandled();

        if (apiResponse == null) {
            return;
        }

        switch (apiResponse.status) {
            case LOADING:
                loginBtn.startAnimation(() -> null);
                break;

            case SUCCESS:
                loginBtn.revertAnimation(() -> null);
                loginBtn.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_edittext));

                navigateToHome();
                break;

            case ERROR:
                loginBtn.revertAnimation(() -> null);
                loginBtn.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_edittext));


                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
                builder.setTitle(getResources().getString(R.string.error_dialog_login_title));
                builder.setMessage(Objects.requireNonNull(apiResponse.error).getMessage());
                builder.setNegativeButton(getResources().getText(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
                break;
            default:
                break;
        }

    }

    @OnClick(R.id.login_btn)
    public void login() {
        this.viewModel.login();
    }

    @OnClick(R.id.register_text)
    public void navigateToRegister() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.forgot_text)
    public void navigateToForgotPassword() {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    void navigateToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    void navigateToVerify() {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        intent.setAction(KeyConstant.ACTION_VERIFY_ACCOUNT);
        intent.putExtra(KeyConstant.ACTIVITY_NAME_BUNDLE_ID, TAG);
        intent.putExtra(KeyConstant.USERNAME, viewModel.username.getValue());
        startActivity(intent);
    }

    void handleVerifyAccount(Event<Boolean> event) {
        Boolean isNavigate = event.getContentIfNotHandled();

        if (isNavigate == null) {
            return;
        }

        navigateToVerify();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginBtn.dispose();
    }
}
