package com.zozo;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDexApplication;

import com.zozo.di.AppComponent;
import com.zozo.di.AppModule;
import com.zozo.di.DaggerAppComponent;
import com.zozo.di.ServiceModule;

public class ZozoApplication extends MultiDexApplication {

    AppComponent appComponent;
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).serviceModule(new ServiceModule()).build();
    }

    public  AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
